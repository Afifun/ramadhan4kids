Untuk step2 running project-nya sbb:

pre-requisites:
- python 3.4.x (terbaru)
- setting path environment python (C:/Python34) & Scripts (C:/Python34/Scripts) 

install django:

-  Buka cmd, kemudian ketik command 'pip install Django==1.8.4', enter, tunggu sampai selesai

install modul xlwt + xlrd:
- extract xlwt+xlrd.zip, setelah selesai akan ditemukan 2 folder xlrd-0.9.3 dan xlwt-future-0.8.0
- buka folder xlrd-0.93 via cmd, cari file setup.py, kemudian jalankan 'python setup.py install', enter, tunggu sampai selesai
- lakukan hal yang sama pada folder xlwt-future-0.8.0

runserver:
- extract ramadahn4kids.zip,
- cari file manage.py, kemudian jalankan degan perintah 'python manage.py runserver' via cmd
- buka browser, ketik url http://localhost:8000