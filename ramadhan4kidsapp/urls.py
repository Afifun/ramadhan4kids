"""ramadhan4kidssite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import login, logout
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve as static_serve
from .views import admin_views, user_views

urlpatterns = [
     url(r'^admin/', include(admin.site.urls)),
     url(r'^administrasi/$', admin_views.index, name='admin.index'),
     url(r'^administrasi/upload-keuangan/$', admin_views.upload_pengeluaran_excel, name='admin.upload_laporan'),
     url(r'^administrasi/laporan-keuangan/$', admin_views.laporan_keuangan, name='admin.laporan_keuangan'),
     url(r'^administrasi/laporan-list/$', admin_views.laporan_list, name='admin.laporan_list'),
     url(r'^administrasi/add-laporan/$', admin_views.add_laporan, name='admin.add.laporan'),
     url(r'^administrasi/edit-laporan/(?P<id>\d+)/$', admin_views.edit_laporan, name='admin.edit.laporan'),
     url(r'^administrasi/kirim-lpj/$', admin_views.kirim_lpj, name='admin.kirim.lpj'),
     url(r'^administrasi/upload-data-donasi/$', admin_views.upload_donasi, name='admin.upload.donasi'),

     url(r'^$', user_views.index, name='user.index'),
     url(r'^login/$', login, name='login'),
     url(r'^logout/$', logout, {'next_page' : '/'}, name='logout'),
     url(r'^story/$', user_views.story, name='user.story'),
     url(r'^keranjang-donasi/$', user_views.keranjang, name='user.keranjang'),
     url(r'^add-item', user_views.add_item, name='user.add_item'),
     url(r'^konfirmasi-donasi/$', user_views.konfirmasi, name='user.konfirmasi'),
     url(r'^sukses/$', user_views.sukses_simpan_donasi, name='user.sukses'),
     url(r'^checkout-donasi/$', user_views.checkout, name='user.checkout'),
     url(r'^isi-identitas-donatur/$', user_views.isi_data, name='user.isi_data'),
     url(r'^remove-item-keranjang/$', user_views.remove_item_keranjang, name='user.remove'),
     

] + static(settings.DATA_URL, document_root=settings.DATA_ROOT)
