from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password


class LocalAuth(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        try:
            user = User.objects.get(username=username)
            password_is_valid = check_password(password, user.password)

            if password_is_valid:
                return user
            else:
                return None

        except (User.DoesNotExist, User.MultipleObjectsReturned) as e:
            print(e)
            return None
