from django.contrib import admin
from .models import *


class ProgramAdmin(admin.ModelAdmin):
    list_display = ('tema', 'tahun',)

admin.site.register(Program, ProgramAdmin)
admin.site.register(TipeDonasi)
admin.site.register(Bank)
admin.site.register(Laporan)
