import time
import datetime

from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from ramadhan4kidsapp.models import *
from ramadhan4kidsapp.models import Laporan
from ramadhan4kidsapp.models import Donatur
from django.core.mail import *
from django.template.loader import get_template
from django.http import JsonResponse
from django.conf import settings
from ramadhan4kidsapp import utils


def index(request):
    try:
        data = {'donasi_list': request.session['item_keranjang'],
                'donasi_list_count': request.session['item_keranjang_count'],
                'current_tipe_donasi': utils.get_current_paket()}
    except Exception as e:
        data = {'current_tipe_donasi' : utils.get_current_paket()}
    return render(request, 'ramadhan4kidsapp/user/index.html', data)


def story(request):

    try:
        laporan_list = Laporan.objects.all().order_by('-id')
        data = { 'story_list': laporan_list}
    except KeyError as e:
        data = {}
    return render(request, 'ramadhan4kidsapp/user/story.html', data)


def add_item(request):
    data = {}
    if request.method == 'GET':
        tipe = request.GET.get('tipe')

        valid = tipe == 'a' or tipe == 'b' or tipe == 'c'

        if valid:
            donasi = tipe

        try:
            if request.session['item_keranjang'] == '' or request.session['item_keranjang'] == None:
                request.session['item_keranjang'] = donasi
            else:
                request.session['item_keranjang'] = request.session['item_keranjang'] + ',' + donasi
            request.session['item_keranjang_count'] += 1
        except KeyError:
            # cart is empty
            request.session['item_keranjang'] = tipe
            request.session['item_keranjang_count'] = 1

        data = { 'donasi_list': request.session['item_keranjang'], 'donasi_list_count': request.session['item_keranjang_count']}

    return render(request, 'ramadhan4kidsapp/user/index.html', data)


def keranjang(request):
    try:
        if request.session['item_keranjang'] != '':
            lst = string_to_list(request.session['item_keranjang'])
            list_donasi = []

            list_item_jumlah = element_counter(lst)

            for item,jumlah in list_item_jumlah:
                tipe_donasi = TipeDonasi.objects.get(tipe=item,program=utils.get_current_program())
                list_donasi.append(Donasi(tipe_donasi=tipe_donasi,jumlah=jumlah))

            data = { 'donasi_list_count' : request.session['item_keranjang_count'],
                     'list_donasi' : list_donasi}
        else:
            data = {'error': 'Keranjang Anda masih kosong. Silakan pilih paket Anda kembali <a href="%s">disini</a>!'
                             % (reverse('ramadhan4kidsapp:user.index') + '#service')}
    except Exception as e:
        data = {'error': 'Sepertinya Anda belum memilih paket. '
                         'Silakan pilih paket <a href="%s">disini</a>!'
                         % (reverse('ramadhan4kidsapp:user.index') + '#service')}

    return  render(request, 'ramadhan4kidsapp/user/keranjang.html', data)


def konfirmasi(request):

    if request.method == 'POST':
        kode = request.POST.get('kode')
        email = request.POST.get('email')
        bank = request.POST.get('bank')
        bukti = request.FILES['bukti']

        if bukti_is_valid(bukti.name):
            is_valid = kode_is_valid(email, kode, bank)
            if is_valid:
                send_mail_confirmation_success(email,'')
                return  render(request, 'ramadhan4kidsapp/user/konfirmasi-respon.html', {'sukses' : True})
            else:
                return  render(request, 'ramadhan4kidsapp/user/konfirmasi-respon.html', {'sukses' : False})
        else :
            pass

    data = {'banks': utils.get_current_banks()}

    try:
        data.update({ 'donasi_list' : request.session['item_keranjang'],
                 'donasi_list_count' : request.session['item_keranjang_count']})
    except Exception as e:
        pass

    return  render(request, 'ramadhan4kidsapp/user/konfirmasi.html', data)


def bukti_is_valid(nama_file):
    list_nama = nama_file.split('.')
    extention = list_nama[len(list_nama) - 1]
    return (extention == 'JPG' or extention == 'jpg' or extention == 'jpeg' or extention == 'JPEG' or extention == 'png' or extention == 'PNG' or extention == 'GIF' or extention == 'gif' or extention == 'pdf' or extention == 'PDF')


def kode_is_valid(email_donatur, kode,bank_id):
    valid = True
    try:
        donasi_donatur = DonasiDonatur.objects.get(kode=kode)
        bank = Bank.objects.get(id=bank_id)

        if not donasi_donatur:
            valid = False
        elif donasi_donatur.is_konfirmasi:
            valid = False
        else:
            donasi_donatur.is_konfirmasi = True
            donasi_donatur.bank_yg_dituju = bank
            donasi_donatur.save()

    except Donatur.DoesNotExist:
        valid = False
    except DonasiDonatur.DoesNotExist:
        valid = False
    except Exception as e:
        valid = False

    return valid


def checkout(request):
    try:
        if request.session['item_keranjang'] != '':
            list = string_to_list(request.session['item_keranjang'])
            list_donasi = []

            identitas_valid = request.session['nama'] != '' and request.session['email'] != '' and request.session['no_hp'] != ''

            if not identitas_valid:
                return render(reverse('ramadhan4kidsapp:user.isi_data'))

            list_item_jumlah = element_counter(list)
            current_program = utils.get_current_program()
            if not current_program:
                data = {'error': 'mohon maaf tidak dapat melakukan aksi ini karena saat ini donasi belum dibuka.'}
                return render(request, 'ramadhan4kidsapp/user/checkout.html', data)

            for item,jumlah in list_item_jumlah:
                tipe_donasi = TipeDonasi.objects.get(tipe=item,program=current_program)
                list_donasi.append(Donasi(tipe_donasi=tipe_donasi, jumlah=jumlah, program=current_program))

            data = { 'nama_donatur':  request.session['nama'],
                     'email_donatur':  request.session['email'],
                     'no_hp_donatur':  request.session['no_hp'],
                     'donasi_list_count' : request.session['item_keranjang_count'],
                     'list_donasi' : list_donasi}

            if request.method == 'POST':
                simpan = request.POST.get('simpan')
                sukses = False
                if simpan == '1':
                    nama = request.session['nama']
                    email_donatur = request.session['email']
                    no_hp_donatur = request.session['no_hp']

                    millis = int(round(time.time() * 1000))
                    tanggal = datetime.date.today()

                    donatur = Donatur(nama=nama, email=email_donatur, no_hp=no_hp_donatur)
                    donatur.save()

                    donasi_donatur = DonasiDonatur(kode=str(millis), tanggal=tanggal, donatur=donatur)
                    donasi_donatur.save()
                    sukses = simpan_donasi(list_donasi, donasi_donatur)

                    plaintext = get_template('ramadhan4kidsapp/mail/checkout_success.html')

                    total_donasi = 0

                    for item in list_donasi:
                        total_donasi += item.total_nominal
		    
                    method = 'https' if request.is_secure() else 'http'
                    host = request.META.get('HTTP_HOST')
                    konfirmasi_link = method + '://' + host + reverse('ramadhan4kidsapp:user.konfirmasi') + "?email=" + donatur.email + "&kode=" + donasi_donatur.kode

                    d = {'nama_donatur': donatur.nama, 'list_donasi': list_donasi,
                         'total_donasi': total_donasi, 'list_rekening' : utils.get_current_banks(),
                         'kode_donasi': donasi_donatur.kode, 'link' : konfirmasi_link}
                    text_content = plaintext.render(d)

                    msg                  = EmailMessage('[Ramadhan For Kids] - Donasi', text_content, 'noreply@ramadhanforkids.com', [donatur.email])
                    msg.content_subtype  = "html"
                    # msg.attach_alternative(html_content, "text/html")
                    msg.send()

                    #send_mail to donatur
                if sukses:
                    del request.session['item_keranjang']
                    del request.session['item_keranjang_count']
                    del request.session['nama']
                    del request.session['email']
                    del request.session['no_hp']
                    return redirect('/sukses/')
        else:
            return redirect(reverse('ramadhan4kidsapp:user.keranjang'))

    except KeyError:
        data = {'error': 'Sepertinya Anda belum memilih paket atau data belum lengkap. '
                          'Silakan pilih paket <a href="%s">disini</a> atau lengkapi data Anda <a href="%s">disini</a>!'
                          % (reverse('ramadhan4kidsapp:user.index') + '#service', reverse('ramadhan4kidsapp:user.isi_data'))}

    return render(request, 'ramadhan4kidsapp/user/checkout.html', data)


def simpan_donasi(donasi_list, donasi_donatur):
    try:
        for item in donasi_list:
            item.donasi_donatur = donasi_donatur
            item.save()
        return True
    except Exception as e:
        return False


def sukses_simpan_donasi(request):
    banks = Bank.objects.filter(program=utils.get_current_program())
    return render(request, 'ramadhan4kidsapp/user/sukses.html', {'banks': banks})


def isi_data(request):
    data = {}
    try:
        if not request.session['item_keranjang_count']:
            data.update({'error': 'Sepertinya Anda belum memilih paket. '
                             'Silakan pilih paket <a href="%s">disini</a>!'
                             % (reverse('ramadhan4kidsapp:user.index') + '#service')})
    except KeyError:
        data.update({'error': 'Sepertinya Anda belum memilih paket. '
                         'Silakan pilih paket <a href="%s">disini</a>!'
                         % (reverse('ramadhan4kidsapp:user.index') + '#service')})

    if request.method == 'POST':
        nama = request.POST.get('nama')
        email = request.POST.get('email')
        no_hp = request.POST.get('no_hp')

        request.session['nama'] = nama
        request.session['email'] = email
        request.session['no_hp'] = no_hp

        return redirect('/checkout-donasi/')

    return  render(request, 'ramadhan4kidsapp/user/form-donatur.html', data)


def element_counter(input_list):
    list_unique = list(set(input_list))
    list_item_jumlah = []
    for item in list_unique:
        list_item_jumlah.append((item, input_list.count(item)))

    return list_item_jumlah


def string_to_list(input_string):
    return sorted(input_string.split(','))


def list_to_string(input_list):
    result = ''
    for ii, item in enumerate(input_list):
        result += str(item)
        if ii < len(input_list) - 1:
            result += ","
    return result


def remove_item_list_by_value(input_list, value):
    while value in input_list:
        input_list.remove(value)
    return input_list


def remove_item_keranjang(request):
    tipe = request.GET.get('tipe')

    if not tipe:
        return redirect('/keranjang-donasi/')

    keranjang_item_list =string_to_list(request.session['item_keranjang'])

    tipe = str(tipe)
    reminder_item_count = int(request.session['item_keranjang_count']) - keranjang_item_list.count(tipe)
    keranjang_item_list = remove_item_list_by_value(keranjang_item_list, tipe)
    request.session['item_keranjang'] = list_to_string(keranjang_item_list)
    request.session['item_keranjang_count'] = reminder_item_count

    return JsonResponse({'item_keranjang': keranjang_item_list, 'item_keranjang_count': reminder_item_count})


def send_mail_confirmation_success(email, donatur_name):

    plaintext = get_template('ramadhan4kidsapp/mail/confirmation_success.html')

    d = {'nama_donatur': donatur_name}
    text_content = plaintext.render(d)

    msg = EmailMessage('[Ramadhan For Kids] - Terima Kasih Sudah Berdonasi',
                       text_content, 'noreply@ramadhanforkids.com', [email])
    msg.content_subtype = "html"
    msg.send()

