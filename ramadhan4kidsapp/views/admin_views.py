__author__ = 'afifun'

import os
import datetime
from datetime import time
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext, Context
from django.utils import timezone
from ramadhan4kidsapp.models import *

@login_required()
def index(request):
    try:
        donasi_list = Donasi.objects.all()
    except Donasi.DoesNotExist:
        donasi_list = None

    data = {'donasi_list': donasi_list}
    return render(request, 'ramadhan4kidsapp/admin/index-admin.html', data)

def upload_laporan(request):
    return  render(request, 'ramadhan4kidsapp/admin/upload-laporan-admin.html')

def laporan_keuangan(request):
    return  render(request, 'ramadhan4kidsapp/admin/laporan-keuangan-admin.html')

def upload_donasi(request):
    if request.method == 'POST':
        namafile = request.FILES['file']
        temp = Temp(filecontent=namafile)
        temp.save()
        path = str(temp.filecontent)
        print(path)
        upload_donasi_excel(path)

        os.remove(path)
        temp.delete()

    return  render(request, 'ramadhan4kidsapp/admin/upload-data-donasi.html')

def upload_donasi_excel(path):
    b = xlrd.open_workbook(path)
    sheet = b.sheet_by_index(0)
    nrows = sheet.nrows
    ncols = sheet.ncols

    for r in range(4, nrows):
        tanggal 		 = sheet.cell_value(r,2)
        print(tanggal)
        tanggal 	     = datetime.datetime.utcfromtimestamp((tanggal - 25569) * 86400.0).strftime('%Y-%m-%d')
        nama 	         = sheet.cell_value(r,3)
        email 	         = sheet.cell_value(r,4)
        paket 	         = sheet.cell_value(r,5).lower()

        print (paket)
        pembagi = 100
        if paket == 'a':
            pembagi = 50
        elif paket == 'b':
            pembagi = 100
        elif paket == 'c':
            pembagi =150
        else:
            raise Exception

        jumlah_paket     = sheet.cell_value(r,6) / pembagi

        bank             = sheet.cell_value(r,7)
        no_hp 	         = sheet.cell_value(r,8)

        donatur = Donatur(nama=nama,email=email, no_hp=no_hp)
        donatur.save()
        millis = int(round(time.time() * 1000))
        donasi_donatur = DonasiDonatur(kode=str(millis), tanggal=tanggal, donatur=donatur)
        donasi_donatur.save()

        tipe_donasi = TipeDonasi.objects.get(tipe=paket)
        donasi = Donasi(tipe_donasi=tipe_donasi, jumlah=jumlah_paket, donasi_donatur=donasi_donatur)

        donasi.save()

def laporan_keuangan(request):
    pengeluaran = Pengeluaran.objects.all()
    data = {'pengeluaran_list':pengeluaran, 'total_pengeluaran': get_total_pengeluaran()}
    return render(request, 'ramadhan4kidsapp/admin/laporan-keuangan-admin.html', data)
def upload_pengeluaran_excel(request):

    if request.method == 'POST':
        namafile = request.FILES['file']
        temp = Temp(filecontent=namafile)
        temp.save()
        path = str(temp.filecontent)
        print(path)
        upload_excel(path)

        os.remove(path)
        temp.delete()

    # path = str(temp.filecontent)

    return  render(request, 'ramadhan4kidsapp/admin/upload-laporan-admin.html')

def upload_excel(path):
    b = xlrd.open_workbook(path)
    sheet = b.sheet_by_index(0)
    nrows = sheet.nrows
    ncols = sheet.ncols

    for r in range(4, nrows):
        tanggal 		 = sheet.cell_value(r,1)
        tanggal 	     = datetime.datetime.utcfromtimestamp((tanggal - 25569) * 86400.0).strftime('%Y-%m-%d')
        nominal 	     = sheet.cell_value(r,2)
        keterangan 	     = sheet.cell_value(r,3)

        untuk_donasi    = False
        if sheet.cell_value(r,4) == 1:
            untuk_donasi = True

        tuple = (tanggal,nominal,keterangan,untuk_donasi)
        print (tuple)

        pengeluaran      = Pengeluaran(tanggal=tanggal,keterangan=keterangan,jumlah=nominal,untuk_donasi=untuk_donasi)
        pengeluaran.save()

def get_total_pengeluaran():
    list_pengeluaran = Pengeluaran.objects.all()
    total = 0
    for item in list_pengeluaran:
        if item.jumlah != None:
            total += item.jumlah
    return total

def get_saldo():
    return get_pemasukan_value() - get_pengeluaran_value()

# Create your views here.
def laporan_list(request):
    laporan_list = Laporan.objects.all()
    return render(request, 'ramadhan4kidsapp/admin/laporan-list.html', {'laporan_list':laporan_list})

def add_laporan(request):
    data = {}
    if request.method == 'POST':
        program = request.POST.get('program')
        judul = request.POST.get('judul')
        content = request.POST.get('konten')
        foto_list = request.FILES.getlist('foto')
        caption_list = request.POST.getlist('caption')

        program_obj = Program.objects.get(pk=program)

        laporan = Laporan(program=program_obj,title=judul, content=content, posted_time=timezone.now())
        laporan.save()
        for i,foto in enumerate(foto_list):
            foto = Foto(laporan=laporan,gambar=foto, caption=caption_list[i])
            foto.save()
    else:
        # initialize program choice form
        data.update({'programs' : Program.objects.all().order_by('-tahun')})

    return render(request, 'ramadhan4kidsapp/admin/laporan-add.html', data)

def edit_laporan(request,id):

    data = {}
    if request.method == 'POST':
        judul = request.POST.get('judul')
        content = request.POST.get('konten')
        foto_list = request.FILES.getlist('foto')
        caption_list = request.POST.getlist('caption')
        delete_foto_list = request.POST.getlist('id-foto')

        laporan = Laporan.objects.get(id=id)
        laporan.title = judul
        laporan.content = content
        laporan.edited = True
        laporan.posted_time = timezone.now()

        laporan.save()
        for i,foto in enumerate(foto_list):
            foto = Foto(laporan=laporan,gambar=foto, caption=caption_list[i])
            foto.save()

        if delete_foto_list:
            for id in delete_foto_list:
                foto = Foto.objects.get(id=id)
                foto.delete()

    else:
        laporan = Laporan.objects.get(id=id)
        data = {'laporan': laporan}

    return render(request, 'ramadhan4kidsapp/admin/laporan-edit.html', data)

def kirim_lpj(request):
    donatur         = Donatur.objects.all()

    d               = Context({'host': '{{ host }}', 'nama_donatur':'{{ nama_donatur }}' } )

    email_subject   = "Laporan Pertanggung Jawaban Ramadhan For Kids"

    data            = {'subjek': email_subject, 'donatur': donatur, 'nama_donatur':'{{ nama_donatur }}'}

    if request.method == 'POST':
        email_list      = []
        email_list_clr  = []
        email_fail      = ''
        success_count   = 0
        try:

            email_subject   = request.POST.get('subjek')
            email_donatur   = request.POST.get('alamat_email')
            email_lainnya   = request.POST.get('email_lainnya')
            attach          = request.FILES['file']


            if(request.POST.get('kirim') == '1'):
                email_all_donatur = []
                for d in donatur:
                    email_all_donatur.append(d.email)
                email_list = email_all_donatur
            else :
                if not validator.is_empty(email_donatur):
                    email_donatur_list = email_donatur.split(";")
                    email_list += email_donatur_list

            if not validator.is_empty(email_lainnya):
                email_lainnya_list   = email_lainnya.split(";")
                email_list          += email_lainnya_list

            txt_content     = request.POST.get('pesan')
            text_file       = open(settings.PROJECT_PATH + "/data/template/temp.html", "w", encoding='utf-8')
            text_file.write(txt_content)
            text_file.close()


            for e in email_list:
                if (not validator.is_empty(e)) and validator.is_email(e):
                    email_list_clr.append(e)

            for e in email_list_clr:
                email   = []
                e       = e.strip()
                if (not validator.is_empty(e)) and validator.is_email(e):
                    email.append(e)

                    nama = ""
                    try:
                        donatur_p   = get_object_or_404(Donatur, email=e)
                        nama        = donatur_p.nama
                    except Exception as e:
                        print(e)

                    d_temp               = Context({'nama_donatur': nama,'program': program, 'host': 'http://localhost:8000', 'donasi': donasi, 'rekening': rekening})
                    template_html_temp   = get_template("temp.html")
                    html_content_temp    = template_html_temp.render(d_temp)
                    # msg                  = EmailMultiAlternatives(email_subject, html_content_temp, 'moh.afifun@ui.ac.id', email)
                    # msg.attach_alternative(html_content_temp, "text/html")
                    msg                  = EmailMessage(email_subject, html_content_temp, 'moh.afifun@ui.ac.id', email, headers={'From': 'beribuku@gmail.com'})
                    msg.content_subtype  = "html"

                    if attach != None:
                        msg.attach(attach.name, attach.read(), attach.content_type)

                    print(email)
                    msg.send()
                    print('sukses')
                    success_count  += 1
                else:
                    email_fail     += e + ','
                print('kosong')

        except Exception as e:
            print(e)

        gagal = len(email_list_clr)-success_count

        print(str(success_count))
        print(str(gagal))
        print(str(len(email_list_clr)))
        print('fail :' +  email_fail)

        data = {'program': program, 'message': html_content_temp, 'subjek': email_subject, 'donatur': donatur, 'sukses': success_count, 'gagal': gagal, 'nama_donatur':'{{ nama_donatur }}'}

    return render(request, 'ramadhan4kidsapp/admin/kirim-lpj.html', data)
