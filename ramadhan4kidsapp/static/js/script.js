$(document).ready(function(){

	$('.btn-gambar').click(
		function(e){
			$('.div-gambar').append(
			'<div class="row"><div class="col-sm-3"><input type="file" class="form-control" name="foto" id="" placeholder=""><p class="help-block">Unggah file gambar dengan jenis jpg, png, atau gif</p></div><div class="col-sm-9"><input type="text" class="form-control" name="caption" id="" placeholder="Keterangan gambar (opsional)"><a href="#" class="remove">Remove</a></div></div>');
	});

	$('.div-gambar').on("click", ".remove", function(e){
		$(this).parent('div').parent('div').remove();
	});

	$('.btn-item').click(
		function(e){
			$('.div-item').append('<div><div class="col-sm-6"><input type="text" name="barang"  class="form-control" id="" placeholder="Item"></div><div class="col-sm-6"><input type="text" name="jumlahBarang" class="form-control" id="" placeholder="Jumlah (optional)"><a href="#" class="remove">Remove</a></div></div>');
	});

	$('.div-item').on("click", ".remove", function(e){
		$(this).parent('div').parent('div').remove();
	});

});

    function addToCart(item) {
        $.ajax({
            url: '/add-item?tipe=' + item,
            success: incrementTotalKeranjangItem(),
        });
    }


    function incrementTotalKeranjangItem(){
        document.getElementById('keranjang-counter-desktop').textContent =
                (parseInt(document.getElementById('keranjang-counter-desktop').textContent.toString()) + 1).toString()

        document.getElementById('keranjang-counter-mobile').textContent =
                (parseInt(document.getElementById('keranjang-counter-mobile').textContent.toString()) + 1).toString()

    }


    window.onload = updateTotalNominal();

    function updateTotalNominalByType(tipe, nominal_satuan, id_cell_total_nominal, id_cell_kuantitas) {
        incrementTotalKeranjangItem();
        var current_kuantitas = parseInt(document.getElementById(id_cell_kuantitas).value);
        var total_nominal_e = document.getElementById(id_cell_total_nominal);
        total_nominal_e.innerHTML = accounting.formatMoney(current_kuantitas * parseInt(nominal_satuan),  "Rp", 2, ".", ",");
        updateTotalNominal();
        addToCart(tipe);
    }

    function updateTotalNominal() {
        var total_nominal_elements = document.getElementsByName("total-nominal-tipe");
        var total = 0;
        for (var ii=0; ii < total_nominal_elements.length; ii++){
            nominal = parseInt(total_nominal_elements[ii].innerHTML.replace('Rp','').replace(',00','').replace('.',''))
            console.log(nominal);
            total += nominal;
        }
        var total_nominal = document.getElementById("total-nominal");
        total = accounting.formatMoney(total,  "Rp", 2, ".", ",");
        total_nominal.innerHTML = '<strong>' + total + '</strong>';
    }

    function updateTotalNominalAndTotalKeranjangItem(jumlahPaketTipe){
        document.getElementById('keranjang-counter-desktop').textContent =
                (parseInt(document.getElementById('keranjang-counter-desktop')
                    .textContent.toString()) - parseInt(jumlahPaketTipe)).toString();


        document.getElementById('keranjang-counter-mobile').textContent =
                (parseInt(document.getElementById('keranjang-counter-mobile')
                    .textContent.toString()) - parseInt(jumlahPaketTipe)).toString();
        updateTotalNominal();
    }

    function removeKeranjangItem(r, tipe) {

        var jumlahPaketTipe = document.getElementById('jumlah-paket-tipe-'.concat(tipe)).value;

        var i = r.parentNode.parentNode.rowIndex;
        document.getElementById("table-keranjang").deleteRow(i);

        $.ajax({
            url: '/remove-item-keranjang?tipe=' + tipe,
            success: updateTotalNominalAndTotalKeranjangItem(jumlahPaketTipe),
        });


    }