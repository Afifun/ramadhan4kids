from django.db import models

class Temp(models.Model):
    filecontent = models.FileField(upload_to='data/file_temp')

# Create your models here.
class PenerimaManfaat(models.Model):
    nama = models.CharField(max_length=100)
    pic = models.CharField(max_length=100)
    jumlah_anak = models.IntegerField(default=0)

class KategoriAnak(models.Model):
    kategori = models.IntegerField()
    keterangan = models.CharField(max_length=100)

class Anak(models.Model):
    kategori = models.ForeignKey(KategoriAnak)
    nama = models.CharField(max_length=100)
    umur = models.IntegerField()
    kelas = models.IntegerField()
    komunitas = models.ForeignKey(PenerimaManfaat)

# Create your models here.
class Bank(models.Model):
    nama_bank = models.CharField(max_length=100)
    nama_pemilik_rek = models.CharField(max_length=100)
    no_rekening = models.CharField(max_length=100)
    program = models.ForeignKey('Program', null=True)
    def __str__(self):
        return "%s | %s" % (self.nama_bank, self.program)

class Donatur(models.Model):
    nama  = models.CharField(max_length=100)
    email = models.CharField(max_length=100, null=True)
    no_hp = models.CharField(max_length=100, null=True)

class TipeDonasi(models.Model):
    tipe = models.CharField(max_length=1)
    nama = models.CharField(max_length=100, null=True)
    nominal = models.IntegerField()
    program = models.ForeignKey('Program', null=True)

    def __str__(self):
        return "%s | %s" % (self.tipe, self.nominal)

class DonasiDonatur(models.Model):
    kode = models.CharField(max_length=20)
    tanggal = models.DateField()
    bank_yg_dituju = models.ForeignKey(Bank, null=True)
    donatur  = models.ForeignKey(Donatur)
    is_konfirmasi = models.BooleanField(default=False)

class Donasi(models.Model):
    tipe_donasi = models.ForeignKey(TipeDonasi)
    jumlah = models.IntegerField()
    donasi_donatur = models.ForeignKey(DonasiDonatur)
    program = models.ForeignKey('Program', null=True)

    @property
    def total_nominal(self):
        return self.jumlah * self.tipe_donasi.nominal

class Program(models.Model):
    tema = models.CharField(max_length=200)
    tahun = models.CharField(max_length=4, primary_key=True)

    def __str__(self):
        return "%s | %s" % (self.tahun, self.tema)

# Create your models here.
class Pengeluaran(models.Model):
    tanggal = models.DateField()
    keterangan = models.TextField()
    jumlah = models.IntegerField()
    untuk_donasi = models.BooleanField()

# Create your models here.
class Laporan(models.Model):
    posted_time = models.DateTimeField()
    edited = models.BooleanField(default=False)
    title = models.CharField(max_length=500)
    content = models.TextField()
    program = models.ForeignKey('Program', null=True)

    def __str__(self):
       return "%s | %s" % (self.title, self.program)

class Foto(models.Model):
    laporan = models.ForeignKey(Laporan, on_delete=models.CASCADE)
    gambar = models.FileField(upload_to='data/media/foto')
    caption = models.CharField(max_length=500)
