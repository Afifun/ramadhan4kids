from django.utils import timezone
from .models import TipeDonasi, Program, Bank


def get_current_paket():
    return get_paket_by_year(get_current_year())


def get_paket_by_year(year):
    program = get_program_by_year(year)

    if not program:
        return None

    tipe_donasi = TipeDonasi.objects.filter(program=program).order_by('tipe')
    return tipe_donasi


def get_current_program():
    return get_program_by_year(get_current_year())


def get_program_by_year(year):
    try:
        program = Program.objects.get(tahun=year)
        return program
    except Exception as e:
        return None


def get_current_year():
    return timezone.now().year

def get_current_banks():
    return get_banks_by_program(program=get_current_program())

def get_banks_by_program(program):
    return Bank.objects.filter(program=program)
