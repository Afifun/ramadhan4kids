--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: administrator_akun; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE administrator_akun (
    id integer NOT NULL,
    username character varying(100) NOT NULL,
    password character varying(100) NOT NULL
);


ALTER TABLE public.administrator_akun OWNER TO ramadhan4kids;

--
-- Name: administrator_akun_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE administrator_akun_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administrator_akun_id_seq OWNER TO ramadhan4kids;

--
-- Name: administrator_akun_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE administrator_akun_id_seq OWNED BY administrator_akun.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO ramadhan4kids;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO ramadhan4kids;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO ramadhan4kids;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO ramadhan4kids;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO ramadhan4kids;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO ramadhan4kids;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO ramadhan4kids;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO ramadhan4kids;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO ramadhan4kids;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO ramadhan4kids;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO ramadhan4kids;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO ramadhan4kids;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO ramadhan4kids;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO ramadhan4kids;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO ramadhan4kids;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO ramadhan4kids;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO ramadhan4kids;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO ramadhan4kids;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_anak; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_anak (
    id integer NOT NULL,
    nama character varying(100) NOT NULL,
    umur integer NOT NULL,
    kelas integer NOT NULL,
    kategori_id integer NOT NULL,
    komunitas_id integer NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_anak OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_anak_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_anak_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_anak_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_anak_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_anak_id_seq OWNED BY ramadhan4kidsapp_anak.id;


--
-- Name: ramadhan4kidsapp_bank; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_bank (
    id integer NOT NULL,
    nama_bank character varying(100) NOT NULL,
    nama_pemilik_rek character varying(100) NOT NULL,
    no_rekening character varying(100) NOT NULL,
    program_id character varying(4)
);


ALTER TABLE public.ramadhan4kidsapp_bank OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_bank_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_bank_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_bank_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_bank_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_bank_id_seq OWNED BY ramadhan4kidsapp_bank.id;


--
-- Name: ramadhan4kidsapp_donasi; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_donasi (
    id integer NOT NULL,
    jumlah integer NOT NULL,
    donasi_donatur_id integer NOT NULL,
    program_id character varying(4),
    tipe_donasi_id integer NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_donasi OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_donasi_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_donasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_donasi_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_donasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_donasi_id_seq OWNED BY ramadhan4kidsapp_donasi.id;


--
-- Name: ramadhan4kidsapp_donasidonatur; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_donasidonatur (
    id integer NOT NULL,
    kode character varying(20) NOT NULL,
    tanggal date NOT NULL,
    is_konfirmasi boolean NOT NULL,
    bank_yg_dituju_id integer,
    donatur_id integer NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_donasidonatur OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_donasidonatur_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_donasidonatur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_donasidonatur_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_donasidonatur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_donasidonatur_id_seq OWNED BY ramadhan4kidsapp_donasidonatur.id;


--
-- Name: ramadhan4kidsapp_donatur; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_donatur (
    id integer NOT NULL,
    nama character varying(100) NOT NULL,
    email character varying(100),
    no_hp character varying(100)
);


ALTER TABLE public.ramadhan4kidsapp_donatur OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_donatur_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_donatur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_donatur_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_donatur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_donatur_id_seq OWNED BY ramadhan4kidsapp_donatur.id;


--
-- Name: ramadhan4kidsapp_foto; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_foto (
    id integer NOT NULL,
    gambar character varying(100) NOT NULL,
    caption character varying(500) NOT NULL,
    laporan_id integer NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_foto OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_foto_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_foto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_foto_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_foto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_foto_id_seq OWNED BY ramadhan4kidsapp_foto.id;


--
-- Name: ramadhan4kidsapp_kategorianak; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_kategorianak (
    id integer NOT NULL,
    kategori integer NOT NULL,
    keterangan character varying(100) NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_kategorianak OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_kategorianak_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_kategorianak_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_kategorianak_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_kategorianak_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_kategorianak_id_seq OWNED BY ramadhan4kidsapp_kategorianak.id;


--
-- Name: ramadhan4kidsapp_laporan; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_laporan (
    id integer NOT NULL,
    posted_time timestamp with time zone NOT NULL,
    edited boolean NOT NULL,
    title character varying(500) NOT NULL,
    content text NOT NULL,
    program_id character varying(4)
);


ALTER TABLE public.ramadhan4kidsapp_laporan OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_laporan_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_laporan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_laporan_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_laporan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_laporan_id_seq OWNED BY ramadhan4kidsapp_laporan.id;


--
-- Name: ramadhan4kidsapp_penerimamanfaat; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_penerimamanfaat (
    id integer NOT NULL,
    nama character varying(100) NOT NULL,
    pic character varying(100) NOT NULL,
    jumlah_anak integer NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_penerimamanfaat OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_penerimamanfaat_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_penerimamanfaat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_penerimamanfaat_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_penerimamanfaat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_penerimamanfaat_id_seq OWNED BY ramadhan4kidsapp_penerimamanfaat.id;


--
-- Name: ramadhan4kidsapp_pengeluaran; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_pengeluaran (
    id integer NOT NULL,
    tanggal date NOT NULL,
    keterangan text NOT NULL,
    jumlah integer NOT NULL,
    untuk_donasi boolean NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_pengeluaran OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_pengeluaran_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_pengeluaran_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_pengeluaran_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_pengeluaran_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_pengeluaran_id_seq OWNED BY ramadhan4kidsapp_pengeluaran.id;


--
-- Name: ramadhan4kidsapp_program; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_program (
    tema character varying(200) NOT NULL,
    tahun character varying(4) NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_program OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_temp; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_temp (
    id integer NOT NULL,
    filecontent character varying(100) NOT NULL
);


ALTER TABLE public.ramadhan4kidsapp_temp OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_temp_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_temp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_temp_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_temp_id_seq OWNED BY ramadhan4kidsapp_temp.id;


--
-- Name: ramadhan4kidsapp_tipedonasi; Type: TABLE; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE TABLE ramadhan4kidsapp_tipedonasi (
    id integer NOT NULL,
    tipe character varying(1) NOT NULL,
    nama character varying(100),
    nominal integer NOT NULL,
    program_id character varying(4)
);


ALTER TABLE public.ramadhan4kidsapp_tipedonasi OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_tipedonasi_id_seq; Type: SEQUENCE; Schema: public; Owner: ramadhan4kids
--

CREATE SEQUENCE ramadhan4kidsapp_tipedonasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ramadhan4kidsapp_tipedonasi_id_seq OWNER TO ramadhan4kids;

--
-- Name: ramadhan4kidsapp_tipedonasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramadhan4kids
--

ALTER SEQUENCE ramadhan4kidsapp_tipedonasi_id_seq OWNED BY ramadhan4kidsapp_tipedonasi.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY administrator_akun ALTER COLUMN id SET DEFAULT nextval('administrator_akun_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_anak ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_anak_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_bank ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_bank_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_donasi ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_donasi_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_donasidonatur ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_donasidonatur_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_donatur ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_donatur_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_foto ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_foto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_kategorianak ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_kategorianak_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_laporan ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_laporan_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_penerimamanfaat ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_penerimamanfaat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_pengeluaran ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_pengeluaran_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_temp ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_temp_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_tipedonasi ALTER COLUMN id SET DEFAULT nextval('ramadhan4kidsapp_tipedonasi_id_seq'::regclass);


--
-- Data for Name: administrator_akun; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY administrator_akun (id, username, password) FROM stdin;
\.


--
-- Name: administrator_akun_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('administrator_akun_id_seq', 1, false);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add user	2	add_user
5	Can change user	2	change_user
6	Can delete user	2	delete_user
7	Can add permission	3	add_permission
8	Can change permission	3	change_permission
9	Can delete permission	3	delete_permission
10	Can add group	4	add_group
11	Can change group	4	change_group
12	Can delete group	4	delete_group
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add bank	7	add_bank
20	Can change bank	7	change_bank
21	Can delete bank	7	delete_bank
22	Can add pengeluaran	8	add_pengeluaran
23	Can change pengeluaran	8	change_pengeluaran
24	Can delete pengeluaran	8	delete_pengeluaran
25	Can add donasi donatur	9	add_donasidonatur
26	Can change donasi donatur	9	change_donasidonatur
27	Can delete donasi donatur	9	delete_donasidonatur
28	Can add penerima manfaat	10	add_penerimamanfaat
29	Can change penerima manfaat	10	change_penerimamanfaat
30	Can delete penerima manfaat	10	delete_penerimamanfaat
31	Can add program	11	add_program
32	Can change program	11	change_program
33	Can delete program	11	delete_program
34	Can add anak	12	add_anak
35	Can change anak	12	change_anak
36	Can delete anak	12	delete_anak
37	Can add laporan	13	add_laporan
38	Can change laporan	13	change_laporan
39	Can delete laporan	13	delete_laporan
40	Can add foto	14	add_foto
41	Can change foto	14	change_foto
42	Can delete foto	14	delete_foto
43	Can add tipe donasi	15	add_tipedonasi
44	Can change tipe donasi	15	change_tipedonasi
45	Can delete tipe donasi	15	delete_tipedonasi
46	Can add donatur	16	add_donatur
47	Can change donatur	16	change_donatur
48	Can delete donatur	16	delete_donatur
49	Can add kategori anak	17	add_kategorianak
50	Can change kategori anak	17	change_kategorianak
51	Can delete kategori anak	17	delete_kategorianak
52	Can add temp	18	add_temp
53	Can change temp	18	change_temp
54	Can delete temp	18	delete_temp
55	Can add donasi	19	add_donasi
56	Can change donasi	19	change_donasi
57	Can delete donasi	19	delete_donasi
58	Can add akun	20	add_akun
59	Can change akun	20	change_akun
60	Can delete akun	20	delete_akun
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('auth_permission_id_seq', 60, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$36000$ALgra8c19MCi$jI/6dzpz/J5niWWzLSwh1kIdHZHe7a0QjI2CcUVUC84=	2017-05-24 15:08:57.115285+07	t	afifun				t	t	2017-05-07 21:47:19.290976+07
2	pbkdf2_sha256$36000$FdGkuXX1h7zL$nKieW7m3zXdo6ZEioZZR3KR49wlL8mUHfrp3c0sdUuI=	2018-05-09 06:12:40.062777+07	t	admin				t	t	2017-05-20 14:29:27.396044+07
3	pbkdf2_sha256$36000$kXNr2bTaKKGW$YOIv3XW+OKcYJ/wrQJiyuoh0QASg7N6MT81/m0mOGvU=	2018-05-11 02:11:34.753865+07	t	afifunnaily				t	t	2018-05-04 09:47:17.480205+07
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('auth_user_id_seq', 3, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2017-05-08 08:51:42.460967+07	2016	Program object	1	[{"added": {}}]	11	1
2	2017-05-09 11:42:22.927988+07	2017	2017 | Happy Little Soul	1	[{"added": {}}]	11	1
3	2017-05-12 17:50:59.204816+07	4	a | 100000	1	[{"added": {}}]	15	1
4	2017-05-12 17:54:47.160648+07	4	a | 100000	3		15	1
5	2017-05-20 14:32:52.395254+07	5	A | 75000	1	[{"added": {}}]	15	2
6	2017-05-20 14:33:13.912728+07	6	B | 100000	1	[{"added": {}}]	15	2
7	2017-05-20 14:33:40.224148+07	7	C | 150000	1	[{"added": {}}]	15	2
8	2017-05-20 14:39:52.015808+07	3	BCA | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	7	2
9	2017-05-20 14:40:01.266006+07	1	BNI Syariah | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	7	2
10	2017-05-20 18:03:03.749013+07	7	c | 150000	2	[{"changed": {"fields": ["tipe"]}}]	15	1
11	2017-05-20 18:03:07.710558+07	6	c | 100000	2	[{"changed": {"fields": ["tipe"]}}]	15	1
12	2017-05-20 18:03:19.350814+07	5	a | 75000	2	[{"changed": {"fields": ["tipe"]}}]	15	1
13	2017-05-20 18:03:31.480991+07	6	b | 100000	2	[{"changed": {"fields": ["tipe"]}}]	15	1
14	2017-05-20 19:42:42.011613+07	2	bank test | 2017 | Happy Little Soul	1		7	2
15	2017-05-20 19:43:06.525148+07	2	Test Bank | 2017 | Happy Little Soul	2	Changed nama_bank and nama_pemilik_rek.	7	2
16	2017-05-21 12:24:09.968427+07	5	A | 75000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
17	2017-05-21 12:24:21.393442+07	6	B | 100000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
18	2017-05-21 12:24:31.33967+07	7	C | 150000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
19	2017-05-21 16:27:31.602642+07	7	c | 150000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
20	2017-05-21 16:27:35.491046+07	6	c | 100000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
21	2017-05-21 16:27:39.561726+07	5	c | 75000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
22	2017-05-21 16:27:45.61506+07	7	c | 150000	2	[]	15	2
23	2017-05-21 16:27:50.014327+07	6	b | 100000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
24	2017-05-21 16:27:53.959504+07	5	a | 75000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
25	2017-05-21 16:36:33.634153+07	2	BCA | 2017 | Happy Little Soul	2	[{"changed": {"fields": ["nama_bank", "nama_pemilik_rek", "no_rekening"]}}]	7	2
26	2017-05-21 16:42:29.534773+07	4	Mandiri | 2017 | Happy Little Soul	1	[{"added": {}}]	7	2
27	2017-05-21 16:44:57.090512+07	5	BNI | 2017 | Happy Little Soul	1	[{"added": {}}]	7	2
28	2017-05-21 16:45:21.919495+07	6	BSM | 2017 | Happy Little Soul	1	[{"added": {}}]	7	2
29	2017-05-21 16:45:45.050047+07	7	CIMB Niaga | 2017 | Happy Little Soul	1	[{"added": {}}]	7	2
30	2017-05-26 11:40:05.29079+07	5	a | 150000	2	[{"changed": {"fields": ["nama", "nominal"]}}]	15	2
31	2017-05-26 11:40:13.154956+07	6	b | 100000	2	[]	15	2
32	2017-05-26 11:40:26.837558+07	7	c | 75000	2	[{"changed": {"fields": ["nama", "nominal"]}}]	15	2
33	2018-05-04 09:54:00.235345+07	16	test	3		13	3
34	2018-05-04 09:54:00.274432+07	15	adfads	3		13	3
35	2018-05-04 09:54:00.27528+07	14	adfads	3		13	3
36	2018-05-04 09:54:00.27611+07	13	adfads	3		13	3
37	2018-05-04 09:55:57.098187+07	7	Ramadhan for (Purworejo) Kids | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	13	3
38	2018-05-04 09:55:59.599815+07	7	Ramadhan for (Purworejo) Kids | 2016 | Happy with Islam	2	[]	13	3
39	2018-05-04 09:56:08.556384+07	6	Bingkisan untuk Adik-Adik Kampung Gedong | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	13	3
40	2018-05-04 09:56:16.312507+07	5	Ramadhan For Kids untuk Anak Yatim RS Dharmais | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	13	3
41	2018-05-04 09:56:28.779478+07	4	Cerita Ramadhan for Kids di Cirebon | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	13	3
42	2018-05-04 09:56:36.695985+07	3	Ramadhan for SD Manggarai Selatan 03 | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	13	3
43	2018-05-04 09:56:43.832491+07	2	Berbuka Puasa Bersama Komunitas Sahabat Anak | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	13	3
44	2018-05-04 09:56:51.246842+07	1	Berbagi Bersama Komunitas Jendela | 2016 | Happy with Islam	2	[{"changed": {"fields": ["program"]}}]	13	3
45	2018-05-08 16:40:01.926216+07	208	208 | 1000 Langkah Penuh Berkah	1	[{"added": {}}]	11	2
46	2018-05-08 16:41:23.930294+07	2018	2018 | 1000 Langkah Penuh Berkah	2	[{"changed": {"fields": ["tahun"]}}]	11	2
47	2018-05-08 16:41:32.104318+07	208	208 | 1000 Langkah Penuh Berkah	3		11	2
48	2018-05-08 16:41:52.043354+07	8	BNI Syariah | 2018 | 1000 Langkah Penuh Berkah	1	[{"added": {}}]	7	2
49	2018-05-08 16:42:38.587703+07	9	BCA | 2018 | 1000 Langkah Penuh Berkah	1	[{"added": {}}]	7	2
50	2018-05-08 16:43:23.002185+07	10	Mandiri | 2018 | 1000 Langkah Penuh Berkah	1	[{"added": {}}]	7	2
51	2018-05-08 16:47:16.384541+07	8	A | 50000	1	[{"added": {}}]	15	2
52	2018-05-08 16:47:30.310224+07	9	B | 125000	1	[{"added": {}}]	15	2
53	2018-05-08 18:10:15.525717+07	2014	2014 | 1000 Langkah Penuh Berkah	2	[{"changed": {"fields": ["tahun"]}}]	11	2
54	2018-05-08 18:10:25.862433+07	2014	2014 | 1000 Langkah Penuh Berkah	3		11	2
55	2018-05-09 06:12:58.553658+07	8	a | 50000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
56	2018-05-09 06:13:06.180531+07	9	b | 125000	2	[{"changed": {"fields": ["tipe"]}}]	15	2
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 56, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	user
3	auth	permission
4	auth	group
5	contenttypes	contenttype
6	sessions	session
7	ramadhan4kidsapp	bank
8	ramadhan4kidsapp	pengeluaran
9	ramadhan4kidsapp	donasidonatur
10	ramadhan4kidsapp	penerimamanfaat
11	ramadhan4kidsapp	program
12	ramadhan4kidsapp	anak
13	ramadhan4kidsapp	laporan
14	ramadhan4kidsapp	foto
15	ramadhan4kidsapp	tipedonasi
16	ramadhan4kidsapp	donatur
17	ramadhan4kidsapp	kategorianak
18	ramadhan4kidsapp	temp
19	ramadhan4kidsapp	donasi
20	administrator	akun
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('django_content_type_id_seq', 20, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2017-05-07 20:25:30.26886+07
2	auth	0001_initial	2017-05-07 20:25:31.049187+07
3	admin	0001_initial	2017-05-07 20:25:31.234311+07
4	admin	0002_logentry_remove_auto_add	2017-05-07 20:25:31.258155+07
5	administrator	0001_initial	2017-05-07 20:25:31.306137+07
6	contenttypes	0002_remove_content_type_name	2017-05-07 20:25:31.378324+07
7	auth	0002_alter_permission_name_max_length	2017-05-07 20:25:31.516621+07
8	auth	0003_alter_user_email_max_length	2017-05-07 20:25:31.642539+07
9	auth	0004_alter_user_username_opts	2017-05-07 20:25:31.660853+07
10	auth	0005_alter_user_last_login_null	2017-05-07 20:25:31.695478+07
11	auth	0006_require_contenttypes_0002	2017-05-07 20:25:31.701855+07
12	auth	0007_alter_validators_add_error_messages	2017-05-07 20:25:31.723001+07
13	auth	0008_alter_user_username_max_length	2017-05-07 20:25:31.882274+07
14	ramadhan4kidsapp	0001_initial	2017-05-07 20:25:33.002788+07
15	sessions	0001_initial	2017-05-07 20:25:33.145272+07
16	ramadhan4kidsapp	0002_auto_20170507_1331	2017-05-07 20:31:24.531017+07
17	ramadhan4kidsapp	0003_auto_20170507_1337	2017-05-07 20:37:30.2256+07
18	ramadhan4kidsapp	0004_auto_20170507_1341	2017-05-07 20:41:07.095234+07
19	ramadhan4kidsapp	0005_laporan_program	2017-05-08 09:16:49.380326+07
20	ramadhan4kidsapp	0006_tipedonasi_program	2017-05-12 08:10:12.087503+07
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('django_migrations_id_seq', 20, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
twnu41bzmr6low14vljztccuis4ttf8b	NWVjYzY1ZjNmZTM0MDM3NWI3NjRmMjdhZTE3MDEzY2U0ZDFkZTNiMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYyxhLGIiLCJpdGVtX2tlcmFuamFuZ19jb3VudCI6M30=	2017-06-04 16:33:32.067512+07
9d7pvgbsw75nkbpr0p85lcfpo5wocv6z	YmRmMTdhMjhhNGE5OGVhODFmNzQwYWNjM2ZmMWZjMjdkOTVmZTVhZDp7Iml0ZW1fa2VyYW5qYW5nIjoiIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjB9	2017-06-11 10:37:01.41541+07
ae7i5j48rzd6g92u9wvi8mzn2yn5m5mr	ZWQ3OGI0MzljMGU1ZDQ4OTA3ZGU3NWViNWQyMjU5MDgxNjVhYzkzMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2018-05-09 09:34:57.074006+07
v8tz98j3rrvzej7ebml0tjjen4s7e7kd	YzgyYzc1NGFkZWEyNWUwODcxZWNiNDExM2NlN2ZmNzFiNGYxZjRmOTp7fQ==	2017-06-04 17:57:06.720351+07
qmh6iv11r5p5kzyshulvyvb98bg0vnuz	ZWQ3OGI0MzljMGU1ZDQ4OTA3ZGU3NWViNWQyMjU5MDgxNjVhYzkzMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2017-06-11 09:59:29.105932+07
ayb1wpoekl7gvvd3ux67srk0vno5q5qb	MzQ0NzMwZjUwYzBlNjdjMGY2MDc5ZjhjMTJhNDk0MjA4MjdmZWFiODp7Iml0ZW1fa2VyYW5qYW5nIjoiYiIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2017-06-11 09:59:29.109002+07
qaypov6ucv1yqbwq46qxsyyk7339b4ja	NzYyZDg1YzUxN2M3ODNhYjdkZTg5MTMyN2RmOTVkNGY2ZjA1MzM4Mjp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo2LCJfYXV0aF91c2VyX2JhY2tlbmQiOiJyYW1hZGhhbjRraWRzYXBwLmF1dGhzLkxvY2FsQXV0aCIsIl9hdXRoX3VzZXJfaGFzaCI6IjE5MWQ4YWRlODEzOGFiMTdhNDQ0NTg1ZjI5YWE1NzYyMzMyOWU4MDkiLCJfYXV0aF91c2VyX2lkIjoiMSIsIml0ZW1fa2VyYW5qYW5nIjpbImEiLCJhIiwiYSIsImEiLCJiIiwiYyJdfQ==	2017-05-22 10:27:29.692769+07
mhz6e1kuhxus3xm5phun792wz5l7ee8o	MWQwODVlNWFlMzRkZjk3MzY5MDA2YjM5MjM1Y2NhMzViOTFjNzNlZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6InJhbWFkaGFuNGtpZHNhcHAuYXV0aHMuTG9jYWxBdXRoIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDA0NmYyODA3NzI2YmViZWI2ZDFhOWIzNDRjZmFhZGE3ODQ4NDkyNCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2017-06-06 13:41:28.556304+07
c6cdr7yn9z6fkdzuzl1qrzloj4951b2x	ZWQ3OGI0MzljMGU1ZDQ4OTA3ZGU3NWViNWQyMjU5MDgxNjVhYzkzMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2017-06-11 09:59:29.50305+07
50btyqum8vzkx89x90k4ya9xd0aim96y	NWIyMzZkMTg0YmYyYTRiYmZlZGYxOGRiZmM1NTVhNjdmMzIyMTMwYTp7Iml0ZW1fa2VyYW5qYW5nIjoiYSxhIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjJ9	2017-06-05 14:52:21.71497+07
csqbhsgqgqypcus2oxq777zyet87c76c	YzgyYzc1NGFkZWEyNWUwODcxZWNiNDExM2NlN2ZmNzFiNGYxZjRmOTp7fQ==	2017-06-06 13:43:49.938231+07
x88nipv60b5syn9l6qlxp0gcoik15awk	NjNhYTI3MGE5Y2U1MDU0MGZiMzBmODE2NGI3NTQxMjk1MDJjYTZmMDp7Iml0ZW1fa2VyYW5qYW5nIjpudWxsLCJpdGVtX2tlcmFuamFuZ19jb3VudCI6OH0=	2017-05-22 10:47:34.321489+07
6f0db20i5y9yjhs3bkyozf3orug6py3g	Njg1NDNlY2NmNmViYWUxZDQ0ZTAzMTYzMTE0Mzk3MDcwYTllYjRiZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoicmFtYWRoYW40a2lkc2FwcC5hdXRocy5Mb2NhbEF1dGgiLCJfYXV0aF91c2VyX2hhc2giOiIxOTFkOGFkZTgxMzhhYjE3YTQ0NDU4NWYyOWFhNTc2MjMzMjllODA5In0=	2017-06-07 15:08:57.126651+07
h0xf4t608yyy4vb4c2a40cgqxecpd562	NmY0MjViMDM2ZjlkZWJjZTkyYjU3ZGNmODMyMjczNDNmODE3YjdlZDp7Iml0ZW1fa2VyYW5qYW5nIjoiYSxjIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjJ9	2017-06-11 09:59:31.151219+07
au1iil0h1pzn415e1n526kwonmz3acxe	MmNiNmUxNTY2ZmNkNzk1N2IyYTAyNTIyMjRiZWY5N2ZlNmE0MDc2ZDp7Iml0ZW1fa2VyYW5qYW5nIjoiYSxiLGMsYSxhIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjV9	2017-06-05 15:20:52.128097+07
or04y67jiyct31cx8netijnk9egq30gu	YTgyMDYxODkwZGE1ODRmNzQ4ZTllNTc4N2QzNTI2NDRjZWNhNmM3MDp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo0LCJpdGVtX2tlcmFuamFuZyI6bnVsbH0=	2017-05-22 10:51:53.000067+07
x7rlyvyg2eeb2ex198hk7lpcqtihjja3	NWI1MDYxMWRhYzM0ZDExNGI0MzZiOGExZGEyNmY3YzgxNWM1MGZjZTp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo1LCJpdGVtX2tlcmFuamFuZyI6ImEsYSxhLGEsYSJ9	2017-05-22 13:45:52.77852+07
x38lxde6glekl38ykmduueex0qcwyh2e	NGVkNzlhM2UxZDBjNzRlZGU0MDYyMmNlYjU0ZGE4YTA0MWZjMmNlZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6InJhbWFkaGFuNGtpZHNhcHAuYXV0aHMuTG9jYWxBdXRoIiwiX2F1dGhfdXNlcl9oYXNoIjoiMTkxZDhhZGU4MTM4YWIxN2E0NDQ1ODVmMjlhYTU3NjIzMzI5ZTgwOSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2017-06-03 18:29:46.870914+07
6ced8mqq9mt7mj9d45fobntwxlpuqpx4	NTE1ZGY2YTI1MWQ1ZWQ4MGQ3YmE4ZWU3ODlmZTRjYjc0MmVlYzg3Mzp7Iml0ZW1fa2VyYW5qYW5nIjoiIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjR9	2017-06-11 09:59:43.29565+07
y9gcrkxm1m4ckfnqn4c7dln9tcwscxo9	NDllYzUzZmFhZmM5MjIzZmY3MWI2MmE0NjNhMGE5YWFlNjEwZWQ3ZTp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo1LCJpdGVtX2tlcmFuamFuZyI6ImEsYixiLGIsYiJ9	2017-05-22 13:58:40.330903+07
oyuif1umc15mko4z7ck1vuhg34vhrzg6	M2Y4ZjQwODhhOTQ0MWY5YjgxM2M5YTE5YzFmN2JiMWI4ZjI0NTUyZTp7Im5hbWEiOiJ3ZXdlIiwiZW1haWwiOiJtb2guYWZpZnVuNDZAZ21haWwuY29tIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjE4LCJub19ocCI6IjE0MTQ1NDMifQ==	2017-05-23 08:37:10.620166+07
cpd2adftx9ellxdmmc3w55kmzj3x9q98	OGUzNmQ3ZjY0ODJiYjI2MTkzNGNmOTRhZmNhMDM0OTZiYTRjZmZkYTp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50IjozfQ==	2017-05-21 21:42:09.052237+07
eiw5bmdop9u0y4tpn4kvfvehsjyeeqr4	NGU3NDEyNWRkMjA1YzcyNjY1YTUzODhiN2ZlYjBhYzA4ZjcyNzEzZjp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo3LCJpdGVtX2tlcmFuamFuZyI6bnVsbH0=	2017-05-22 10:58:27.435075+07
xgct4mqw2jogpoysorssl5yf3t3rr3it	ZWY0ZWZiMmM5ZjVlYTQxYjcyZmRmYTYzYjM2MDU1MmY2NjhkYzM1ZTp7ImVtYWlsIjoibW9oLmFmaWZ1bjQ2QGdtYWlsLmNvbSIsIml0ZW1fa2VyYW5qYW5nIjoiYSxhLGEiLCJub19ocCI6IjkyODI4MiIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjozLCJuYW1hIjoiQWprYSJ9	2017-06-06 07:52:40.789242+07
vlyaa2kg9laaiokukghszppo0x44nsa0	OGMyMWNlNmEzMjZiMWJhNmNiYWJmNDQ3YmJkZGZmZGEyNGNiMzQ0Yjp7Iml0ZW1fa2VyYW5qYW5nIjpudWxsLCJpdGVtX2tlcmFuamFuZ19jb3VudCI6NX0=	2017-05-22 11:02:01.328957+07
290f33p1ehdnlhe8f1tnlfxrdetm0b7y	ZWQyMjVmZWU5Y2Y0M2ViZmI3ZDcwOTFkYWUzMjM4NmMxMzQ5MDljMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYixiLGIsYixjLGMsYyIsIml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo3fQ==	2017-05-23 09:06:44.247585+07
nlekkdx0aijn30rkd0wm1esyxdbs9gmd	MzQ0NzMwZjUwYzBlNjdjMGY2MDc5ZjhjMTJhNDk0MjA4MjdmZWFiODp7Iml0ZW1fa2VyYW5qYW5nIjoiYiIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2017-05-23 11:38:56.562337+07
x4ychyeqpts2896dhu3q4rtm8e8w103s	ZWQ3OGI0MzljMGU1ZDQ4OTA3ZGU3NWViNWQyMjU5MDgxNjVhYzkzMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2017-05-23 11:38:57.48102+07
3chsboer0frbi8p20npvzkcwwktftcg3	ZWQ3OGI0MzljMGU1ZDQ4OTA3ZGU3NWViNWQyMjU5MDgxNjVhYzkzMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2017-05-23 11:39:01.031326+07
8o5g10qkwamm3zvm4j5x6b3apq3f1910	MTNmYjE3NWY4NWJlNjQ3YjRiNzA5OWMyNGYyZDA5NTJlYjg1NjEyZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiaXRlbV9rZXJhbmphbmciOiJhLGMsYyxjLGIsYiIsIl9hdXRoX3VzZXJfaGFzaCI6IjE5MWQ4YWRlODEzOGFiMTdhNDQ0NTg1ZjI5YWE1NzYyMzMyOWU4MDkiLCJpdGVtX2tlcmFuamFuZ19jb3VudCI6NiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoicmFtYWRoYW40a2lkc2FwcC5hdXRocy5Mb2NhbEF1dGgifQ==	2017-05-23 11:43:50.340144+07
r27fc3h4q2b27ojmm6v69944tv1vi2hl	YzgyYzc1NGFkZWEyNWUwODcxZWNiNDExM2NlN2ZmNzFiNGYxZjRmOTp7fQ==	2017-06-06 09:24:21.730731+07
zcwvuuso48rc01272zfwiiepoh9npb5e	YzgyYzc1NGFkZWEyNWUwODcxZWNiNDExM2NlN2ZmNzFiNGYxZjRmOTp7fQ==	2017-06-06 09:28:35.781819+07
d5qpww1p50itqu7nts3t6rtasjn6grtx	ODI5MzZiYjUyNzVmNmNkYjA2ODYwMTA1ZTVmZGZmYWUwMDEyOTJmMTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6InJhbWFkaGFuNGtpZHNhcHAuYXV0aHMuTG9jYWxBdXRoIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiJkMDQ2ZjI4MDc3MjZiZWJlYjZkMWE5YjM0NGNmYWFkYTc4NDg0OTI0In0=	2017-06-03 20:02:49.593497+07
r1j8xi0pasj1qh51la9fpp10k3au3g03	MWQwODVlNWFlMzRkZjk3MzY5MDA2YjM5MjM1Y2NhMzViOTFjNzNlZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6InJhbWFkaGFuNGtpZHNhcHAuYXV0aHMuTG9jYWxBdXRoIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDA0NmYyODA3NzI2YmViZWI2ZDFhOWIzNDRjZmFhZGE3ODQ4NDkyNCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2017-06-04 16:28:26.070109+07
099yvruxdn96cnacxmobwi7ru8sw5i9u	MmY5MjdjZDcwZTZjMWI0NGIxOTQzNTFlODcwZmZkMzM1ZGM0ZWZkZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoicmFtYWRoYW40a2lkc2FwcC5hdXRocy5Mb2NhbEF1dGgiLCJfYXV0aF91c2VyX2hhc2giOiIxOTFkOGFkZTgxMzhhYjE3YTQ0NDU4NWYyOWFhNTc2MjMzMjllODA5IiwiaXRlbV9rZXJhbmphbmciOiJhLGEsYSxhLGEsYSxhLGEsYSxhLGEiLCJpdGVtX2tlcmFuamFuZ19jb3VudCI6MTF9	2017-06-09 04:39:42.885496+07
8ypyhgk3l00ust6ejqghzcboac5yp8mc	ZWQ3OGI0MzljMGU1ZDQ4OTA3ZGU3NWViNWQyMjU5MDgxNjVhYzkzMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2017-06-11 10:04:54.230287+07
oc37b0vtvq8nrfcq0umgb9whaneabd9p	MjllMGE2MjhjNzI0NjNjMWE5YjQzMWFhNTRmZDNmYmIwYzRhNjlkNjp7Iml0ZW1fa2VyYW5qYW5nIjoiYSxiIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjJ9	2017-06-11 10:04:54.298945+07
xa2t1vz9zt0uy3s5vnr2e4n025xob3l4	ZWQ3OGI0MzljMGU1ZDQ4OTA3ZGU3NWViNWQyMjU5MDgxNjVhYzkzMzp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2017-06-11 10:04:54.548301+07
kp5l7yc0el1y5s72p1oc8wqfe7xc552z	NDdlMTU1ZWQ4MTU2ZTQ4YjhlODdjYWE2ZDg5MzljNjAyZjk3YjdiOTp7Iml0ZW1fa2VyYW5qYW5nIjoiYixiIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjJ9	2017-06-11 10:04:54.659597+07
5kw50w1t31976bsv5betxljohq6zomol	YzA0OGQzOGUxMjc5YzQzYWFjYzYyYTljNDMxYzIyZTRmNzQwNDQwZTp7Iml0ZW1fa2VyYW5qYW5nIjoiIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjAsIm5hbWEiOiJBY2htYWQgTmFuZGFwcmF0b21vIiwiZW1haWwiOiJuYW5kYS5wcmF0b21vQGdtYWlsLmNvbSIsIm5vX2hwIjoiMDgyMjk4OTc1NTI3In0=	2018-05-23 14:40:33.041978+07
5595uz11fdace8mu4ggbifwz15fbsvf5	YzAxYTk2OTUyMWRlNDJlYWM5ZjU2MjI5OGM0NjMzY2JkMzM5ODQ0Zjp7Iml0ZW1fa2VyYW5qYW5nIjoiYiIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxLCJuYW1hIjoiU3VjaSBGYWRoaWxhaCIsImVtYWlsIjoic3VjaWZhZGhpbGFoMjNAZ21haWwuY29tIiwibm9faHAiOiIwODU3NjgxMDI5MjcifQ==	2018-05-23 16:16:27.478512+07
xbyu0wotm6r4y17hlhezvi5wldjqpua7	YzgyYzc1NGFkZWEyNWUwODcxZWNiNDExM2NlN2ZmNzFiNGYxZjRmOTp7fQ==	2018-05-25 10:54:32.271751+07
nwnwrrwugayjr4ec1n5wva0aix55ghfw	Mzk2NTZlMDY1NjE0YmMwMjc0ZTcwYzg2ODg2ZDk3NWE4NThjNzUxNjp7Iml0ZW1fa2VyYW5qYW5nIjoiQSxhLGIiLCJpdGVtX2tlcmFuamFuZ19jb3VudCI6M30=	2018-05-23 17:48:03.472137+07
o7k9x8rw3p0kxjpkx33dlhv5xaqpzwzc	MjRjNjFjMjBlZjQ3NzhkNTg5YjFmYmU0Y2Y3MGZjZDZkZGI4ZjAzNjp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo3LCJpdGVtX2tlcmFuamFuZyI6ImEsYyxjLGIsYSxiLGMifQ==	2017-06-11 10:08:30.8204+07
h1dsw6lm9np25keoco5n7x36ajo0yph8	N2QwZDljMTVmMjVhOWQzYjkxYTNlMWQ5ZWM4ODczN2U2NmQ4MWI4MDp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxLCJuYW1hIjoiRmFkaGxhbiIsImVtYWlsIjoiZmFkaGxhbmRyQGdtYWlsLmNvbSIsIm5vX2hwIjoiMDg1NjE5MzYyMjMifQ==	2018-05-23 21:03:54.520092+07
lem6s4jhvu1u0supgwhbzetnx9lr8bv0	YzViMzhiMTliNjNhYjAxMmVhYjIyZTg5Zjk5ZmI2Yzg3NGY1OGY1ZDp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxLCJpdGVtX2tlcmFuamFuZyI6ImEifQ==	2017-06-11 10:09:44.460753+07
datljigivhrnqa2ic741alurygklvfdc	NWIyMzZkMTg0YmYyYTRiYmZlZGYxOGRiZmM1NTVhNjdmMzIyMTMwYTp7Iml0ZW1fa2VyYW5qYW5nIjoiYSxhIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjJ9	2018-05-23 21:34:04.523557+07
pnx0v9t3306pdylpv1n2axuv5h0e3jr9	YjE4MzQ0ZjYxZTZiMmNjMDhiMzYwNTUxYmJjYzVjNzgxYjkzMTBjZjp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo2LCJpdGVtX2tlcmFuamFuZyI6IiJ9	2017-06-11 10:10:13.742843+07
55pe495f3650jfvbkzyoqt63g18dv6q0	YzViMzhiMTliNjNhYjAxMmVhYjIyZTg5Zjk5ZmI2Yzg3NGY1OGY1ZDp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxLCJpdGVtX2tlcmFuamFuZyI6ImEifQ==	2017-06-11 10:15:43.424456+07
11lhc0lp68rf6104jtocyktdjcr4d6fl	N2NlZjExY2IwNDc2NGU2NTM3ZDMzNGQ1ZTYxODI4MjJiNGFiYzhkNjp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50IjowLCJpdGVtX2tlcmFuamFuZyI6IiJ9	2017-06-11 10:26:03.778418+07
45f22hw5e7buijmby23meorxsm37d901	MzQ0NzMwZjUwYzBlNjdjMGY2MDc5ZjhjMTJhNDk0MjA4MjdmZWFiODp7Iml0ZW1fa2VyYW5qYW5nIjoiYiIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2018-05-09 09:35:00.329256+07
ffv75cdghzvaf6qpcx297pj7tajgecmg	MzQ2NDk1NGQ0NTE1YWRkZjNhNzY0NzNmYzkyZDQzZTQ0ZjQ1MGZiOTp7Iml0ZW1fa2VyYW5qYW5nIjoiYyIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2018-05-09 09:35:05.953277+07
dni2wtql5zcpyruct7ifzn5xf8homp57	YTBkOTYxMGM5MmVlYTQ2YjNkNTJmNWEyMmRlZTRjZWRlMDYzOTIwZTp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxNSwiaXRlbV9rZXJhbmphbmciOiJhLGMsYyxiLGIsYixiLGEsYSxhLGEsYSxhLGEsYSJ9	2017-06-11 10:16:17.280232+07
5iphu3xeuetbo894mwvet9colhugx7pt	YWIxNDA1MGU3YzliZDk1NWZmMWE1ZjRhMTBlNTFlNDg1YzhkNzQwZTp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5NzQ4NGQ0YTE2ZTBiMDM4NWJlYjFjYmZiNWRjOGJjOTY1MDYyYTUzIn0=	2018-05-18 09:48:20.521008+07
lde4zxkqkclgr86n3jfud9t8w49plqtd	MGExZTMyY2UxM2Y5MzAwMDQwMWJkYWQ4MDIwZThiMzY0YjZlNGMxMzp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50IjoyLCJpdGVtX2tlcmFuamFuZyI6ImEsYiJ9	2017-06-11 10:17:27.178038+07
lbznvxs8dux9u0z1n1khzsahpq1l5xje	YWQwYTY5ODY1NDA1N2VlNWMyNTRhZGU5YzY0MGZkNWMwNjU4ZTNiZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMDQ2ZjI4MDc3MjZiZWJlYjZkMWE5YjM0NGNmYWFkYTc4NDg0OTI0In0=	2018-05-22 18:07:46.943376+07
wm7ia5pmdyk4oihyvckj2a9ru4r78013	ZDdjODE1MzNlMDNhMzkzYjA2MWNkMjExNzc3OTM1MWZmNGFiMTRmZjp7Iml0ZW1fa2VyYW5qYW5nX2NvdW50Ijo2LCJpdGVtX2tlcmFuamFuZyI6ImEsYixjLGMsYixhIn0=	2017-06-11 10:17:43.948577+07
j24892ra5ho116jdie7w0yan0uycsbee	YWQwYTY5ODY1NDA1N2VlNWMyNTRhZGU5YzY0MGZkNWMwNjU4ZTNiZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMDQ2ZjI4MDc3MjZiZWJlYjZkMWE5YjM0NGNmYWFkYTc4NDg0OTI0In0=	2018-05-23 06:12:40.079551+07
6lmmeiy831n6m5cw3ldbascw25xr4zdg	Nzc5NWE1Y2RhOWM4N2M5NTQ4ZmE1YzM0MjIzZjBmY2RjYjczMjQ3ZDp7Iml0ZW1fa2VyYW5qYW5nIjoiYixiLGIiLCJpdGVtX2tlcmFuamFuZ19jb3VudCI6MywibmFtYSI6IlNhZmlyYSBQdXNwYW5pbmcgQXl1IiwiZW1haWwiOiJzYWZpcmFwdXNwYTE5OTRAZ21haWwuY29tIiwibm9faHAiOiIwODc4ODM0MTU2MjAifQ==	2018-05-23 09:04:57.553276+07
r6s88kw2e9l2ce8xuvuhblpgls9b8tt5	YzMzNjg4MDViMmM4N2NlYjlhY2IxYWJlM2IwYmU4NDRhMzIzYjRhNTp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxLCJuYW1hIjoiTHVsdSBBdWxpYW5kaW5pIFIgIiwiZW1haWwiOiJhdWxpYW5kaW5pckBnbWFpbC5jb20iLCJub19ocCI6IjA4MTIxODA1NzY5NSAifQ==	2018-05-23 09:13:40.149076+07
pj2ifuzg04fqkdzxo2m7k6ua7k0e0nu1	Yzg5ODM5N2NkMDY1YmRjZjI0YWYxMjI5MTBiOThkYTQ1MWRiZWE3Njp7Iml0ZW1fa2VyYW5qYW5nIjoiYSIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxLCJuYW1hIjoiUW9uaXRhIE5hZGlhIiwiZW1haWwiOiJxb25pdGFhbmFkaWFAZ21haWwuY29tIiwibm9faHAiOiIwODk2NzMzNjIwNDQifQ==	2018-05-23 09:42:32.166808+07
5n2ksvqagk4sdp8isymkk8896qe9esmo	YzAxYTk2OTUyMWRlNDJlYWM5ZjU2MjI5OGM0NjMzY2JkMzM5ODQ0Zjp7Iml0ZW1fa2VyYW5qYW5nIjoiYiIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxLCJuYW1hIjoiU3VjaSBGYWRoaWxhaCIsImVtYWlsIjoic3VjaWZhZGhpbGFoMjNAZ21haWwuY29tIiwibm9faHAiOiIwODU3NjgxMDI5MjcifQ==	2018-05-23 09:49:48.680791+07
c7ahj2y0oryw7ux1cwquyusj3fzayi3z	YWNmMDJmODJkMzdmZjc0YTkwN2JmYTQyMGNhYWY4Y2Q2N2NkZjJhYjp7Iml0ZW1fa2VyYW5qYW5nIjoiYSxiIiwiaXRlbV9rZXJhbmphbmdfY291bnQiOjIsIm5hbWEiOiJhZmlmdW4iLCJlbWFpbCI6Im1vaC5hZmlmdW40NkBnbWFpbC5jb20iLCJub19ocCI6IjA4NTYxMjMwOTQ2In0=	2018-05-23 11:01:01.964616+07
1r5n3z1zp4b52qapp0fnq29xx0xskl0w	OTkyNDM0NGMxYjliOWU0NzEwZjJiOWM3ZGY4NDdjYTliMzBmZWJkZDp7Iml0ZW1fa2VyYW5qYW5nIjoiYSxiLGIiLCJpdGVtX2tlcmFuamFuZ19jb3VudCI6MywibmFtYSI6IlEiLCJlbWFpbCI6ImFzdGFyaXFpc3R5QGdtYWlsLmNvbSIsIm5vX2hwIjoiMDg5NTM2MTg0ODgzNSJ9	2018-05-23 12:06:12.078742+07
lp2hwuqj25t1sn9ah24xmp4e71yc1bqo	MzQ0NzMwZjUwYzBlNjdjMGY2MDc5ZjhjMTJhNDk0MjA4MjdmZWFiODp7Iml0ZW1fa2VyYW5qYW5nIjoiYiIsIml0ZW1fa2VyYW5qYW5nX2NvdW50IjoxfQ==	2018-05-23 13:41:26.683917+07
5wrxdxi04bbvicsrb45r4105ex2a3a3w	YWIxNDA1MGU3YzliZDk1NWZmMWE1ZjRhMTBlNTFlNDg1YzhkNzQwZTp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5NzQ4NGQ0YTE2ZTBiMDM4NWJlYjFjYmZiNWRjOGJjOTY1MDYyYTUzIn0=	2018-05-25 02:11:34.785535+07
\.


--
-- Data for Name: ramadhan4kidsapp_anak; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_anak (id, nama, umur, kelas, kategori_id, komunitas_id) FROM stdin;
\.


--
-- Name: ramadhan4kidsapp_anak_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_anak_id_seq', 1, false);


--
-- Data for Name: ramadhan4kidsapp_bank; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_bank (id, nama_bank, nama_pemilik_rek, no_rekening, program_id) FROM stdin;
3	BCA	Dipta Tanaya	5465332082	2016
1	BNI Syariah	Dipta Tanaya	0445382426	2016
2	BCA	Wahyu Retnowati	8691349586	2017
4	Mandiri	Wahyu Retnowati	1570003646370	2017
5	BNI	Wahyu Retnowati	0267040700	2017
6	BSM	Wahyu Retnowati	7108327892	2017
7	CIMB Niaga	Wahyu Retnowati	761163124300	2017
8	BNI Syariah	Safira Puspaning Ayu	0615875760	2018
9	BCA	Diena Fitriya	6825104431	2018
10	Mandiri	Amelia Warliana	9000008125156	2018
\.


--
-- Name: ramadhan4kidsapp_bank_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_bank_id_seq', 10, true);


--
-- Data for Name: ramadhan4kidsapp_donasi; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_donasi (id, jumlah, donasi_donatur_id, program_id, tipe_donasi_id) FROM stdin;
167	1	53	\N	3
168	1	54	\N	1
169	1	54	\N	3
170	1	54	\N	2
171	1	55	\N	2
172	1	56	\N	1
173	1	56	\N	3
174	1	57	\N	1
175	1	58	\N	3
176	1	59	\N	3
177	1	60	\N	3
178	1	61	\N	2
179	1	62	\N	3
180	1	63	\N	3
181	1	63	\N	2
182	1	64	\N	3
183	1	65	\N	3
184	1	66	\N	3
185	1	67	\N	3
186	1	68	\N	3
187	1	69	\N	3
188	1	70	\N	3
189	1	71	\N	3
190	1	72	\N	3
191	1	73	\N	3
192	1	74	\N	1
193	1	74	\N	3
194	1	75	\N	3
195	1	76	\N	1
196	1	77	\N	1
197	1	78	\N	1
198	1	79	\N	3
199	1	80	\N	3
200	1	81	\N	3
201	1	81	\N	2
202	1	82	\N	2
203	1	83	\N	2
204	1	84	\N	1
205	1	84	\N	1
206	1	85	\N	3
207	1	86	\N	2
208	1	87	\N	1
209	1	87	\N	3
210	1	87	\N	2
211	1	88	\N	3
212	1	89	\N	1
213	1	89	\N	1
214	1	89	\N	3
215	1	90	\N	1
216	1	91	\N	3
217	1	92	\N	2
218	1	92	\N	2
219	1	93	\N	3
220	1	94	\N	3
221	1	94	\N	2
222	1	95	\N	3
223	1	95	\N	3
224	1	96	\N	1
225	1	96	\N	2
226	1	97	\N	2
227	1	98	\N	3
228	1	99	\N	3
229	1	100	\N	1
230	1	100	\N	3
231	1	100	\N	2
232	1	101	\N	1
233	1	101	\N	3
234	1	101	\N	2
235	1	102	\N	1
236	1	102	\N	3
237	1	102	\N	2
238	1	103	\N	2
239	1	104	\N	2
240	1	105	\N	1
241	1	105	\N	3
242	1	105	\N	2
243	1	106	\N	2
244	1	107	\N	1
245	1	108	\N	2
246	1	109	\N	1
247	1	109	\N	1
248	1	110	\N	3
249	1	111	\N	3
250	1	111	\N	3
251	1	112	\N	3
252	1	112	\N	2
253	1	113	\N	2
254	1	114	\N	1
255	1	114	\N	3
256	1	114	\N	2
257	1	115	\N	3
258	1	116	\N	3
259	1	117	\N	2
260	1	118	\N	2
261	1	119	\N	3
262	1	120	\N	3
263	1	120	\N	2
264	1	121	\N	2
265	1	122	\N	1
266	1	122	\N	3
267	1	123	\N	2
268	1	124	\N	3
269	1	125	\N	1
270	1	125	\N	2
271	1	126	\N	3
272	1	126	\N	2
273	1	127	\N	1
274	1	128	\N	3
275	1	129	\N	3
276	1	130	\N	1
277	1	130	\N	2
278	1	131	\N	1
279	1	131	\N	3
280	1	131	\N	2
281	1	132	\N	2
282	1	132	\N	2
283	1	132	\N	2
284	1	132	\N	2
285	1	132	\N	2
286	1	133	\N	2
287	1	133	\N	2
288	1	133	\N	2
289	1	133	\N	2
290	1	134	\N	1
291	1	134	\N	2
292	1	135	\N	1
293	1	135	\N	1
294	1	136	\N	1
295	1	136	\N	3
296	1	137	\N	2
297	1	137	\N	2
298	1	137	\N	2
299	1	137	\N	2
300	1	138	\N	2
301	1	139	\N	1
302	1	139	\N	1
303	1	139	\N	3
304	1	140	\N	2
305	1	141	\N	2
306	1	142	\N	3
307	1	142	\N	2
308	1	143	\N	1
309	1	144	\N	1
310	1	144	\N	3
311	1	144	\N	3
312	1	144	\N	3
313	1	145	\N	3
314	1	146	\N	2
315	1	146	\N	2
316	1	147	\N	3
317	1	148	\N	1
318	1	148	\N	2
319	1	149	\N	1
320	1	150	\N	1
321	1	150	\N	3
322	1	151	\N	3
323	1	151	\N	2
324	1	152	\N	3
325	1	153	\N	3
326	1	153	\N	2
16	1	6	\N	2
17	1	6	\N	2
18	1	6	\N	2
19	8	7	\N	3
20	4	7	\N	1
21	6	7	\N	2
22	2	8	2017	7
23	1	8	2017	5
24	4	8	2017	6
25	4	9	2017	7
26	1	9	2017	6
27	1	10	2017	6
28	1	10	2017	7
29	2	10	2017	5
30	1	11	2017	6
31	2	11	2017	7
32	3	11	2017	5
33	3	12	2017	6
34	3	12	2017	5
35	2	12	2017	7
36	2	13	2017	6
37	2	13	2017	7
38	3	13	2017	5
42	2	15	2017	7
43	3	15	2017	5
44	2	15	2017	6
48	2	17	2017	7
49	2	17	2017	5
50	2	17	2017	6
51	2	18	2017	7
52	3	18	2017	5
53	2	18	2017	6
54	3	19	2017	7
55	2	19	2017	5
56	2	19	2017	6
57	1	20	2017	7
58	1	20	2017	5
59	1	20	2017	6
60	1	21	2017	7
61	1	21	2017	5
62	1	21	2017	6
63	1	22	2017	6
64	2	23	2017	6
65	2	23	2017	7
66	2	23	2017	5
75	2	28	2017	5
76	2	28	2017	6
79	2	30	2017	5
80	2	30	2017	6
81	1	31	2017	7
82	1	31	2017	6
83	1	31	2017	5
84	2	32	2017	7
85	2	32	2017	6
86	2	33	2017	7
87	2	33	2017	5
88	2	33	2017	6
89	1	34	2017	7
90	1	34	2017	5
91	1	34	2017	6
92	2	35	2017	5
93	1	35	2017	6
120	2	156	2018	9
121	3	156	2018	8
122	1	157	2018	8
123	3	157	2018	9
124	1	158	2018	9
125	3	158	2018	8
126	1	159	2018	9
127	2	159	2018	8
128	2	160	2018	8
129	3	160	2018	9
130	3	161	2018	8
131	1	161	2018	9
132	2	162	2018	8
133	2	163	2018	9
134	3	163	2018	8
135	3	164	2018	8
136	3	165	2018	8
137	4	166	2018	8
138	1	167	2018	8
\.


--
-- Name: ramadhan4kidsapp_donasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_donasi_id_seq', 138, true);


--
-- Data for Name: ramadhan4kidsapp_donasidonatur; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_donasidonatur (id, kode, tanggal, is_konfirmasi, bank_yg_dituju_id, donatur_id) FROM stdin;
53	1464106642619	2016-05-24	t	1	53
54	1464182461932	2016-05-25	t	3	54
55	1464253458034	2016-05-26	t	1	55
56	1464265843943	2016-05-26	t	3	56
57	1464274869354	2016-05-26	f	\N	57
58	1464300880562	2016-05-26	f	\N	58
59	1464306690092	2016-05-26	t	1	59
60	1464329320940	2016-05-27	t	3	60
61	1464361324900	2016-05-27	f	\N	61
62	1464361793165	2016-05-27	t	3	62
63	1464370469644	2016-05-27	t	3	63
64	1464381732703	2016-05-27	f	\N	64
65	1464381849955	2016-05-27	f	\N	65
66	1464381982195	2016-05-27	f	\N	66
67	1464382045581	2016-05-27	f	\N	67
68	1464382053057	2016-05-27	f	\N	68
69	1464382060561	2016-05-27	f	\N	69
70	1464382060799	2016-05-27	f	\N	70
71	1464382061166	2016-05-27	f	\N	71
72	1464382178035	2016-05-27	f	\N	72
73	1464385717613	2016-05-27	f	\N	73
74	1464429096354	2016-05-28	f	\N	74
75	1464429839105	2016-05-28	t	3	75
76	1464433121116	2016-05-28	t	1	76
77	1464436360747	2016-05-28	f	\N	77
78	1464436377337	2016-05-28	f	\N	78
79	1464439783041	2016-05-28	t	3	79
80	1464455902186	2016-05-28	t	3	80
81	1464475525765	2016-05-28	t	3	81
82	1464534949996	2016-05-29	t	1	82
83	1464586677278	2016-05-30	t	3	83
84	1464591064388	2016-05-30	t	1	84
85	1464605254951	2016-05-30	t	1	85
86	1464624396039	2016-05-30	t	3	86
87	1464750332270	2016-06-01	t	3	87
88	1464834014056	2016-06-02	f	\N	88
89	1464874276953	2016-06-02	t	1	89
90	1465053854956	2016-06-04	f	\N	90
91	1465095887148	2016-06-05	f	\N	91
92	1465136193858	2016-06-05	t	3	92
93	1465142778851	2016-06-05	t	1	93
94	1465168992193	2016-06-05	t	1	94
95	1465186280701	2016-06-06	t	3	95
96	1465195485774	2016-06-06	t	3	96
97	1465198094878	2016-06-06	f	\N	97
98	1465198097563	2016-06-06	t	3	98
99	1465198349514	2016-06-06	f	\N	99
100	1465198369107	2016-06-06	f	\N	100
101	1465198434447	2016-06-06	f	\N	101
102	1465198545488	2016-06-06	f	\N	102
103	1465199303726	2016-06-06	t	1	103
104	1465200273058	2016-06-06	f	\N	104
105	1465204985580	2016-06-06	t	1	105
106	1465216176153	2016-06-06	t	3	106
107	1465248624618	2016-06-06	t	3	107
108	1465258285139	2016-06-07	t	1	108
109	1465271077234	2016-06-07	t	3	109
110	1465273149482	2016-06-07	t	3	110
111	1465281940959	2016-06-07	t	3	111
112	1465290690250	2016-06-07	t	1	112
113	1465306160502	2016-06-07	t	3	113
114	1465311442439	2016-06-07	t	3	114
115	1465315691095	2016-06-07	f	\N	115
116	1465319831953	2016-06-07	t	3	116
117	1465344123226	2016-06-08	f	\N	117
118	1465345866354	2016-06-08	t	3	118
119	1465351297757	2016-06-08	f	\N	119
120	1465363351603	2016-06-08	t	1	120
121	1465373211167	2016-06-08	t	1	121
122	1465376012845	2016-06-08	t	3	122
123	1465378489149	2016-06-08	f	\N	123
124	1465386613941	2016-06-08	t	3	124
125	1465391654056	2016-06-08	f	\N	125
126	1465394799779	2016-06-08	t	3	126
127	1465439985998	2016-06-09	t	1	127
128	1465443239646	2016-06-09	t	1	128
129	1465443984004	2016-06-09	t	1	129
130	1465484091638	2016-06-09	t	1	130
131	1465519428990	2016-06-10	t	3	131
132	1465528575598	2016-06-10	f	\N	132
133	1465529525463	2016-06-10	t	3	133
134	1465530098445	2016-06-10	t	3	134
135	1465531276311	2016-06-10	t	1	135
136	1465531282440	2016-06-10	t	1	136
137	1465555692665	2016-06-10	t	1	137
138	1465573313109	2016-06-10	t	1	138
139	1465597155824	2016-06-10	t	1	139
140	1465615077274	2016-06-11	t	1	140
141	1465616580771	2016-06-11	t	3	141
142	1465632881292	2016-06-11	t	3	142
143	1465640341693	2016-06-11	t	1	143
144	1465654478416	2016-06-11	t	3	144
145	1465796041891	2016-06-13	f	\N	145
146	1465889264839	2016-06-14	t	3	146
147	1466137923159	2016-06-17	t	3	147
148	1467003184129	2016-06-27	t	3	148
149	1467115554680	2016-06-28	t	3	149
150	1468988879068	2016-07-20	f	\N	150
151	1469613548474	2016-07-27	f	\N	151
152	1471059046300	2016-08-13	f	\N	152
153	1471159073338	2016-08-14	f	\N	153
6	1494168128079	2017-05-07	f	\N	6
7	1494293703604	2017-05-09	f	\N	7
8	1495278225960	2017-05-20	f	\N	8
9	1495279786391	2017-05-20	f	\N	9
10	1495282115389	2017-05-20	f	\N	10
11	1495283095397	2017-05-20	t	2	11
12	1495285367220	2017-05-20	t	2	12
13	1495285588250	2017-05-20	f	\N	13
15	1495285913153	2017-05-20	f	\N	15
17	1495286676095	2017-05-20	f	\N	17
18	1495286802295	2017-05-20	t	2	18
19	1495358905523	2017-05-21	t	2	19
20	1495359242937	2017-05-21	f	\N	20
21	1495360016509	2017-05-21	f	\N	21
22	1495364226502	2017-05-21	f	\N	22
23	1495435936236	2017-05-22	t	2	23
28	1495506260957	2017-05-23	f	\N	28
30	1495506463289	2017-05-23	f	\N	30
31	1495506515642	2017-05-23	t	5	31
32	1495506870187	2017-05-23	f	\N	32
33	1495521178540	2017-05-23	f	\N	33
34	1495521687739	2017-05-23	f	\N	34
35	1495521829600	2017-05-23	f	\N	35
156	1525968842578	2018-05-10	f	\N	158
157	1525969462138	2018-05-10	f	\N	159
158	1525969902733	2018-05-10	f	\N	160
159	1525970365550	2018-05-10	t	8	161
160	1525971335028	2018-05-10	t	8	162
161	1525972063495	2018-05-10	f	\N	163
162	1525972341615	2018-05-10	f	\N	164
163	1525972481811	2018-05-10	f	\N	165
164	1525972766946	2018-05-10	f	\N	166
165	1525973358116	2018-05-10	f	\N	167
166	1525973684269	2018-05-10	t	8	168
167	1526010871827	2018-05-11	f	\N	169
\.


--
-- Name: ramadhan4kidsapp_donasidonatur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_donasidonatur_id_seq', 167, true);


--
-- Data for Name: ramadhan4kidsapp_donatur; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_donatur (id, nama, email, no_hp) FROM stdin;
53	Putri Kurniasari	putri.kurniasari92@gmail.com	08999180094
54	NH	nurul.hikmahib@gmail.com	089602565299
55	Ranti Prahyameita	rantip30@gmail.com	085780784239
56	Retno Dyah Anjarwati	dyahanjar331@gmail.com	081328884632
57	Nabhilla	cacha_nayura@yahoo.com	081510945678
58	Anonim	anonim@anonim.com	0123456
59	Arkanty Septyvergy	arkanty.septyvergy@gmail.com	081330555589
60	Hafiz Fakhrurrozy	hafiz.itp45@gmail.com	085692785233
61	Amadea kamami	Amakamami@gmail.com	085254915003
62	Amadea kamami	amadeakamami@yahoo.com	085254915003
63	sabrina	sabrinaagna@yahoo.com	081293731662
64	Fatma 	fatma_haerani@hotmail.co.id	0811442737
65	Andi Chaeruddin	fatma_haerani@hotmail.co.id	0811442737
66	Samindara Hayasmine	fatma_haerani@hotmail.co.id	0811442737
67	Gilang Aldzaqwan	fatma_haerani@hotmail.co.id	0811442737
68	Gilang Aldzaqwan	fatma_haerani@hotmail.co.id	0811442737
69	Gilang Aldzaqwan	fatma_haerani@hotmail.co.id	0811442737
70	Gilang Aldzaqwan	fatma_haerani@hotmail.co.id	0811442737
71	Gilang Aldzaqwan	fatma_haerani@hotmail.co.id	0811442737
72	Gilang Aldzaqwan	fatma_haerani@hotmail.co.id	0811442737
73	Hamba Allah	itsyurachaerani11@gmail.com	089501684102
74	Audra Ferdiansyah	audralovita@gmail.com	081266465597
75	TARI	lestariniaprilia@gmail.com	087894285446
76	Siti Hasna Mursyidah	sitihasnamursyidah@gmail.com	+6283806786930
77	Iva	azzahra.iva@gmail.con	087823621167
78	Iva	azzahra.iva@gmail.con	087823621167
79	Rizka inggita siwi 	rizkainggita29@gmail.com	085248758708 
80	Widi	widirizki@gmail.com	081219110734
81	Nova Adi Kusuma	aknova22@gmail.com	08112803020
82	Fauzan Helmi Sudaryanto	ujan@ujan.org	082299662236
83	Yahya Eru Cakra	yahyaerucakra@gmail.com	085693930416
84	Elvira Susiana	aaiiueoo.el@gmail.com	+6285693314400
85	Fitriah Dwi Indriani	myfitriah@ymail.com	085726138482
86	Sri Ajeng Handayani	sriajengh@yahoo.co.id	081318631171
87	Aditya Amin Refandi	aa.refandi92@gmail.com	08112642006
88	desya	desya.3n@gmail.com	08111992618
89	Gina Andriyani	gina.andriyani.wantania@gmail.com	+6283876092839
90	Riyanti	riyanti1411@gmail.com	081212484232
91	Isnaini Taufiqirahmah	isnainitaufiqirahmah@yahoo.co.id	085640265266
92	Audry Pragita	audrypragita@gmail.com	081905158000
93	Nabilah	nabilah.rosa@gmail.com	085750757836
94	Hamba Allah	royasiaramadani@gmail.com	+6285719272437
95	bina	bina2deci@gmail.com	085798800024
96	Ambar	ambsyarifah@gmail.com	082119756612
97	bukky	bukky.suwarno@lipi.go.id	085659964900
98	Yunny Lestari	yunnylestari@gmail.com	085711333142
99	Mariza Rizqi 	marizarizqi@gmail.com	085640485392
100	Muhammad Harza	muhharza@gmail.com	085790848810
101	Muhammad Harza	muhharza@gmail.com	085790848810
102	Muhammad Harza	muhharza@gmail.com	085790848810
103	Indri Astuti	indri.astuti13@ymail.com	08999402236
104	Tania Jonardi	taniaprissilma@gmail.com	081317821296
105	Mami Istanti	namasayatanty@gmail.com	08111702009
106	Fenti Rahmawati Ningtyas	ningtyas.fenti@gmail.com	085745113759
107	aulia kusumaningtyas	auliakusuma123@gmail.com	085712341371
108	Citra Mutia	tr.citramutiahabib@gmail.com	08197253702
109	Tyas Meuthia	tyas.meuthia@gmail.com	081515155193
110	Ailsa Amelia	ailsa.amelia@yahoo.com	081318932919
111	Intan Danastri	intan.danastri@gmail.com	085692729729
112	Laras	mayaretnoayu@gmail.com	089636806651
113	Khusna Nadia	khusnanadia@gmail.com	085883241117
114	Mukti Unggul Sejati	mukti.unggul.sejati@gmail.com	08976252931
115	Iqbal	iqbalhape@gmail.com	085717004165
116	aprilia	aprilia.nieepo90@gmail.com	081329161945
117	AA	emailyangasik@gmail.com	08994631767
118	RS	riezasalsabila.bogor@gmail.com	087870858725
119	Balda Zain Fauziyyah	baldafsidjabat@gmail.com	082210092416
120	Rizky Jamiatun Muslimah	ryzkiesomnia@gmail.com	085729003919
121	rezia auliannisaa	zhyasapi@gmail.com	085721191575
122	Paramita Widyandari 	mita.widyandari@gmail.com	08159764298
123	ABDULLAH RUMKHUL ISLAM	Skyheadcool@gmail.com	085780655875
124	Putri Permata	putri13596@gmail.com	081281197409
125	Suci Fadhilah	sucifadhilah23@gmail.com	085768102927
126	Isnawaty habsyi	isnahabsyi@gmail.com	083813578568
127	Farah	fauziah.rahmawati7@gmail.com	085890474674
128	betty	aaiiueoo.el@gmail.com	+6281393898634
129	Liqo perindu surga	rizkyramadhani91@yahoo.com	085740729385
130	Ade	adeazurat@gmail.com	081314759574
131	Arlisa	icharel90@yahoo.com	085655696468
132	Diba Anindhita	dibdib_nyunyu@yahoo.com	081808511725
133	Diba Anindhita	dibdib_nyunyu@yahoo.com	081808511725
134	Wita Astari / Lamia Aqila Kya Mecca	weetadicky@gmail.com	08117407007
135	safina	cvmulia.safina@yahoo.com	085203755658
136	Hening	ordinary_235u@yahoo.com	08561651240
137	Abdullah	z.majdy1996@gmail.com	089609298093
138	Indri	indri.handayani1307@gmail.com	08561315387
139	Alia	all.luts@gmail.com	085697464080
140	Riandra Ramadana	riandrar13@gmail.com	+6285717100133
141	Salma Nara	salma.fadhilla@gmail.com	081908007537
142	Hamba Allah	sdwihuripto@gmail.com	+6281281729393
143	Salma Muazaroh	salmadeim@gmail.com	089602564180
144	Nahda Salimah	salimahnahda@gmail.com	085211478420
145	Suci Fadhilah an Kak Iffah	sucifadhilah23@gmail.com	085768102927
146	Ricky Irawan	richie_vasilli@yahoo.com	085228026120
147	Anggraeni	apuspitas01@gmail.com	+628569086679
148	Etika Rezkina	etika.rezkina@gmail.com	085640355998
149	Fara amelia	faranoor17@yahoo.co.id	081315061552
150	afifun	afif123ran@gmail.com	12312312
151	andi	andi.07@gmail.com	082184867200
152	Alexander	alex@haha.com	071672978712
153	Risma Siti Yusniasari	rismasiti.yusniasari@gmail.com	0898943
6	asdfasdf	moh.afifun46@gmail.com	018081023
7	12312	afif123ran@gmail.com	124143
8	afifun	moh.afifun46@gmail.com	81902830918231
9	afifun	moh.afifun46@gmail.com	14235234534
10	afifun naily	moh.afifun46@gmail.com	0192891823
11	afifun naily	moh.afifun46@gmail.com	1241432
12	afifun naily 2	moh.afifun46@gmail.com	120182409214
13	afifun 3	moh.afifun46@gmail.com	1908102983
15	afifun 123	moh.afifun46@gmail.com	0912830192
17	afifun 1234	moh.afifun46@gmail.com	637545443445
18	afifun 5567	moh.afifun46@gmail.com	09128098123
19	afifun test	moh.afifun46@gmail.com	2342352354
20	Adelia Herlisa	aazleeza@gmail.com	085894564609
21	Adelia Herlisa	aazleeza@gmail.com	085894564609
22	Virda	setyani.virda@gmail.com	081332883718
23	afifun	moh.afifun46@gmail.com	109823123
28	sdfsd	moh.afifun46@gmail.com	09283740283
30	sfsd	moh.afifun46@gmail.com	1287913
31	Ade Azurat	adeazurat@gmail.com	081314759574
32	asdfasd	moh.afifun46@gmail.com	909808
33	akjsdfnj	moh.afifun46@gmail.com	89127091823
34	Adelia Herlisa	aazleeza@gmail.com	085894564609
35	Mayatest	mayaretnayu@gmail.com	089517312238
158	afifun	moh.afifun46@gmail.com	+628561230946
159	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
160	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
161	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
162	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
163	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
164	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
165	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
166	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
167	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
168	Moh Afifun Naily	moh.afifun46@gmail.com	+628561230946
169	Tes Web	mayaretnoayu@gmail.com	088291111111
\.


--
-- Name: ramadhan4kidsapp_donatur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_donatur_id_seq', 169, true);


--
-- Data for Name: ramadhan4kidsapp_foto; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_foto (id, gambar, caption, laporan_id) FROM stdin;
1	data/media/foto/20150628_165329.jpg		1
2	data/media/foto/C360_2015-07-01-12-54-14-480.jpg		3
3	data/media/foto/dharmais.JPG		5
4	data/media/foto/IMG-20150628-WA0025.jpg		6
5	data/media/foto/20150621_155204.jpg		2
9	data/media/foto/IMG-20150715-WA0033_TGzrFMc.jpg		7
10	data/media/foto/f71faa6f-6761-497c-a6fc-8a3168ee5091.png		4
\.


--
-- Name: ramadhan4kidsapp_foto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_foto_id_seq', 8, true);


--
-- Data for Name: ramadhan4kidsapp_kategorianak; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_kategorianak (id, kategori, keterangan) FROM stdin;
\.


--
-- Name: ramadhan4kidsapp_kategorianak_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_kategorianak_id_seq', 1, false);


--
-- Data for Name: ramadhan4kidsapp_laporan; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_laporan (id, posted_time, edited, title, content, program_id) FROM stdin;
7	2016-05-10 10:20:26+07	t	Ramadhan for (Purworejo) Kids	<p>Pukul 12.30, satu hari sebelum lebaran, saya tiba di rumah setelah menempuh perjalanan dari Depok selama hampir 24 jam. Rumah tradisional berbentuk joglo itu tetap berdiri kokoh meski hanya disangga oleh tiang-tiang kayu yang sudah berusia puluhan tahun. Saya disambut oleh ibu saya dengan setengah heran melihat barang-barang bawaan saya yang sangat banyak. Saya cium tangannya yang semakin menua itu, saya peluk tubuhnya yang semakin ringkih itu. Saya ceritakan tentang proyek mulia Ramadhan for Kids ini. Ibu mengangguk paham dan malah bersemangat membantu menyebarkan. Dan saya pun (meski sangat lelah) siap menyebarkan berkah Ramadhan kepada anak-anak di Kalijering hari itu juga.</p>\r\n<p>Kalijering adalah sebuah desa terpencil di Kecamatan Pituruh Kabupaten Purworejo. Untuk mencapai desa ini, satu-satunya cara efektif adalah dengan menggunakan kendaraan roda dua. Dari jalan raya kecamatan, desa ini dapat dicapai selama sekitar satu jam dengan menggunakan motor berkecepatan sekitar 20--40 km/jam. Apa kecepatannya tidak bisa ditambah? Jalanan yang naik turun, berbatu, sempit, diapit jurang dan tebing, tidak memungkinkan untuk berkendara dengan kecepatan di atas 40 km/jam.</p>\r\n<p>Hampir semua penduduk Desa Kalijering adalah petani sekaligus peternak. Selain menanam padi dan komoditi seperti cengkeh, kapulaga, kelapa, dan pisang, mereka biasanya juga memelihara sapi, kambing, dan ayam. Tanaman padi mereka biasanya digunakan sebagai bahan makanan pokok mereka, sementara hasil pertanian lain dijual sebagai pemasok ekonomi mereka. Dari hasil penjualan itulah mereka menghidupi keluarga dan menyekolahkan anak-anak mereka, paling tinggi sampai SMA.</p>\r\n<p>Karena besok sudah lebaran, mau tidak mau hari ini juga baju baru dan alat shalat baru ini diberikan kepada yang berhak. Ibu membantu mendaftar siapa saja anak-anak yang berhak menerima. Saya berbagi tugas dengan Ibu tentang siapa yang akan memberikan kepada siapa. Setelah sepakat, saya memulai perjalanan dengan diantar oleh sepupu menggunakan motor.</p>\r\n<p>Sesiang itu, hingga hampir Maghrib kami habiskan waktu untuk berkeliling desa Kalijering. Desa yang terpencil itu ternyata luas juga. Jarak satu rumah dengan rumah lainnya cukup jauh. Namun, karena tidak ada lagi waktu selain hari ini, baik, mari kita selesaikan.</p>\r\n<p>Saya mengetuk pintu demi pintu rumah sederhana yang kadang masih beralaskan tanah. Mengucapkan salam terbaik yang dijawab dengan jawaban terbaik pula. Saya disambut bak malaikat baik hati yang bahkan tidak setiap tahun menyambangi rumah mereka dengan membawa sepercik kebaikan. Ucapan terima kasih dan untaian doa-doa dalam bahasa Jawa mereka berikan berkali-kali setelah saya ceritakan tentang program mulia ini.</p>\r\n<p>Di suatu rumah, tangan kanan anak perempuan berusia enam tahun meraih tangan kanan saya dan menciumnya. Sementara tangan kirinya memegang mukena baru berwarna merah jambu dan baju gamis warna serupa bermotif bunga-bunga. Senyum bahagianya tak bisa ia sembunyikan. Siapa yang menyangka kalau baru kemarin ia merengek meminta mukena yang ada tasnya untuk lebaran esok hari dan ditolak oleh ibunya karena terbatasnya ekonomi mereka. Demikian ibunya bercerita. Dan entah doa apa yang dipanjatkan oleh ibunya tadi malam, hari ini, melalui tangan saya, Allah memberikan keinginan anak kecil itu ditambah baju baru yang mungkin tidak ia bayangkan sebelumnya. Dada saya mulai sesak, mata saya mulai panas, saya hendak menangis, tapi saya tahan. Saya hanya tersenyum dan mengusap kepala anak perempuan itu. Semoga menjadi anak yang shalihah dan pintar, batin saya. Hanya Allah lah satu-satunya Tuhan yang bisa membuat skenario seindah ini. Dia lah satu-satunya yang bisa memberikan rezeki dari arah yang tak terduga. Dan Dia lah satu-satunya yang mampu melembutkan hati yang keras, melapangkan hati yang sempit dan menerangkan hati yang gelap. Dia lah satu-satunya yang memiliki kasih sayang tak terbatas.</p>\r\n<p>Hari semakin sore, gema takbir dan suara tabuhan bedug sudah mulai terdengar. Sebentar lagi pastilah adzan Maghrib berkumandang. Dan benar saja, tidak lebih dari lima menit setelah itu, adzan berkumandang. Ini buka puasa terakhir di Ramadhan kali ini. Saya bergegas pulang sambil sebelumnya melihat tas jinjing yang kini sudah kosong. Artinya, pekerjaan saya sudah selesai. Dalam hati saya sangat bersyukur telah terlibat dalam proyek mulia ini. Seusainya adzan Maghrib itu, saya berdoa sebanyak-banyaknya dan sebaik-baiknya untuk siapapun yang telah terlibat dalam proyek ini. Betapa upaya kecil kita atau uang yang mungkin tak seberapa buat kita, sangat berarti untuk mereka yang sangat membutuhkan. Semoga keberkahan Ramadhan terlimpah untuk anak-anak itu, untuk para donatur dan tentunya untuk kami yang masih belajar menemukan keberkahan-keberkahan Allah di setiap aktivitas kami. Semoga Allah mempertemukan kami kembali dengan Ramadhan berikutnya. (Riyanti)</p>	2016
5	2015-10-15 10:51:32+07	t	Ramadhan For Kids untuk Anak Yatim RS Dharmais	<p><span style="font-weight: 400;"><img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/903/rLWE03.jpg" alt="" width="414" height="234" /><br /></span></p>\r\n<p><span style="font-weight: 400;">Setelah sukses membagikan paket-paket Ramadhan For Kids ke beberapa komunitas di Jakarta dan Depok, kami terus mencari komunitas untuk menjadi penerima manfaat. Dalam masa tersebut, alhamdulillah kami mendapat rekomendasi dari salah satu dokter di Rumah Sakit&nbsp; Kanker Dharmais. Beliau adalah salah seorang dokter di Instalasi Rehabilitasi Medik. Beliau dan beberapa dokter lainnya membina sebanyak 37 anak yatim yang tinggal di sekitar rumah sakit. Beberapa di antara anak-anak yang dibina adalah kerabat dari pasien kanker di rumah sakit. Anak-anak tersebut sebagian besar duduk di bangku SD dan SMP, serta ada beberapa yang sudah duduk di bangku SMA.</span></p>\r\n<p><span style="font-weight: 400;">Alhamdulillah, pada tanggal 10 Juli 2015 sebanyak 37 paket B (37 alat solat, 27 Al Ma'surat dan 10 Al Quran kecil) berhasil sampai di Rumah Sakit Dharmais dan langsung diserahkan kepada pengurus. Sayangnya, beberapa hari menjelang pembagian paket salah seorang pengurus terkena demam berdarah dan harus menjalani rawat inap di rumah sakit hingga terpaksa pembagian paket ditunda hingga setelah lebaran. Namun, kami tetap berdoa semoga tidak mengurangi nilai pahala dan berkah di bulan Ramadhan. </span></p>\r\n<p><span style="font-weight: 400;">Tepat tanggal 16 Juli 2015 pukul 09.00-11.00 WIB, bertepatan dengan acara halal bihalal yang diadakan oleh Rohis Rumah Sakit Kanker Dharmais, paket Ramadhan for Kids berhasil tersampaikan dengan baik. Terdapat sebanyak 37 penerima manfaat dalam acara ini, yaitu anak yatim piatu yang dibina oleh para dokter seperti yang telah dijelaskan sebelumnya. Banyak ucapan terima kasih disampaikan oleh anak-anak sembari mendokan agar para donatur diberi kelimpahan rizki. Selain itu, mereka berharap acara seperti ini dapat diadakan kembali di tahun depan.Semoga berkah Ramadhan setiap tahunnya bisa di rasakan oleh anak-anak yatim lebih luas lagi. Aamiin ya Allah. (Nunung Mulyani)</span></p>	2016
4	2016-05-11 09:43:45+07	t	Cerita Ramadhan for Kids di Cirebon	<p><span style="font-weight: 400;">Batas pengumpulan donasi sudah lewat, tetapi donasi masih terus masuk ke rekening panitia Ramadhan for Kids. Sementara itu, masing-masing dari kami juga harus pulang kampung. Namun, sebagai upaya maksimal yang coba kami lakukan untuk menjalankan amanah ini agar tetap bernilai ibadah di bulan Ramadhan, akhirnya diputuskan bahwa sisa paket yang ada dibagikan di kampung halaman kami dan teman-teman. Salah satunya yaitu di Cirebon.</span></p>\r\n<p><strong><strong>&nbsp;</strong></strong></p>\r\n<p><span style="font-weight: 400;">Di sini banyak sekali target anak-anak yang dapat dijadikan penerima paket Ramadhan for Kids. Mengingat keterbatasan waktu yang ada,akhirnya sebanyak 5 paket dititipkan melalui salah seorang teman anggota komunitas Ramadhan for Kids (Irma namanya) di daerah Celancang, kabupaten Cirebon, untuk dibagikan kepada 5 anak terpilih. Anak-anak terpilih tersebut adalah anak-anak yang memang rajin shalat ke masjid dan memiliki latar belakang sesuai dengan sasaran program Ramadhan for Kids ini. </span></p>\r\n<p><strong><strong>&nbsp;</strong></strong></p>\r\n<p><span style="font-weight: 400;">Ternyata, donasi masih terus masuk. Akhirnya donasi sebanyak kurang lebih Rp 700.000,- dibelikan mukena dan sarung di hari Kamis, 16 Juli 2015 di siang hari sekitar pukul 10.00 WIB di Pasar Kalitanjung, Cirebon. Meski sudah banyak toko yang tutup, akhirnya didapat 10 sarung dan 10 mukena yang siap dibagikan kepada anak-anak. </span></p>\r\n<p><strong><strong>&nbsp;</strong></strong></p>\r\n<p><span style="font-weight: 400;">Sebanyak 5 paket dibagikan kepada anak-anak di daerah Kebon Pelok, Cirebon, setelah adzan Ashar. Anak-anak tersebut baru selesai pulang dari agenda &ldquo;obrog-obrog&rdquo; yang seakan sudah menjadi budaya sebelum hari Raya. Obrog-obrog adalah sejenis agenda meminta sumbangan dalam bentuk beras atau uang, bisa dilakukan sendiri atau berkelompok. </span></p>\r\n<p><strong><strong>&nbsp;</strong></strong></p>\r\n<p><span style="font-weight: 400;">Sembari takbir berkumandang, pawai obor menyambut Hari Raya sudah ramai di mana-mana, setelah shalat Maghrib, sebanyal 15 paket sarung dan mukena dibagikan kepada anak-anak di daerah Pesisir, Kesunean, Cirebon. Kenapa akhirnya dipilih di daerah Pesisir adalah karena kami punya rekan di sana sehingga anak-anak yang sesuai dengan kriteria sasaran bisa langsung dikumpulkan, mengingat keterbatasan waktu yang ada. Juga, di daerah pesisir, anak-anak kurang mendapatkan pendidikan tentang agama. Sehingga diharapkan dengan adanya sentuhan berbagi alat shalat ini, bisa memotivasi mereka untuk belajar mengaji (agama). </span></p>\r\n<p><img src="https://drive.google.com/open?id=0B8bJjuIktoa5aUJkS3dha1lfSms" alt="" /></p>\r\n<p><span style="font-weight: 400;">Saat proses pembagian, wajah anak-anak ini biasa saja. Maklum saja, yang membagikan seperti orang asing bagi mereka. Namun, orang-orang yang kenal dengan salah satu panitia Ramadhan for Kids</span> <span style="font-weight: 400;">menyampaikan bahwa anak-anak sangat senang menerima hadiah lebaran tersebut. Doa pun tak luput disampaikan. Semoga doa-doa baik yang disampaikan, Allah catat dan dijadikan sebagai pahala bagi para donatur. </span></p>\r\n<p><strong><strong>&nbsp;</strong></strong></p>\r\n<p><span style="font-weight: 400;">Demikianlah cerita pembagian paket di Cirebon. Pada akhirnya, begitu banyak anak-anak yang masih kurang beruntung untuk sekedar menikmati baju atau alat shalat baru. Dengan sedikit donasi yang kita sisihkan, ternyata ada &nbsp;anak-anak yang bisa kita buat senyumnya menjadi lebih ceria. Berawal dari Jakarta dan Depok, akhirnya tak sengaja, program Ramadhan for Kids dapat mencapai luar kota seperti Cirebon, Purworejo, bahkan hingga Pacitan. Semoga keberkahan selalu Allah karuniakan pada hidup kita semua. Aamiin. (Nurlatifah)</span></p>\r\n<p>&nbsp;</p>	2016
3	2016-05-04 08:23:16+07	t	Ramadhan for SD Manggarai Selatan 03	<p><span style="font-weight: 400;"><img src="blob:https%3A//drive.google.com/921cebb9-b382-48a3-98a6-f1aa24965bae" alt="Displaying C360_2015-07-01-12-54-14-480.jpg" /><img src="blob:https%3A//drive.google.com/921cebb9-b382-48a3-98a6-f1aa24965bae" alt="Displaying C360_2015-07-01-12-54-14-480.jpg" /><img src="blob:https%3A//drive.google.com/921cebb9-b382-48a3-98a6-f1aa24965bae" alt="Displaying C360_2015-07-01-12-54-14-480.jpg" /><img src="blob:https%3A//drive.google.com/921cebb9-b382-48a3-98a6-f1aa24965bae" alt="" /><img src="blob:https%3A//drive.google.com/2ee00db8-2e27-471b-b213-6072dd35c163" alt="" /><img src="blob:https%3A//drive.google.com/2ee00db8-2e27-471b-b213-6072dd35c163" alt="" /></span></p>\r\n<table style="margin-left: auto; margin-right: auto;">\r\n<tbody>\r\n<tr>\r\n<td><img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/901/sAANjZ.jpg" alt="" width="230" height="172" /></td>\r\n<td style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/911/YCqR89.jpg" alt="" width="227" height="170" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><span style="font-weight: 400;"><br /><br />Ramadhan ke 14 menjadi jadwal bagi tim Ramadhan For Kids membagikan paket untuk Sekolah Dasar di bilangan Manggarai Selatan. Udara pagi yang sejuk saat itu ternyata tak mampu menghilangkan aroma sungai yang ada di depan sekolah itu. Ya, SDN Manggarai Selatan 03 terletak persis di depan sebuah sebuah sungai. Seperti kebanyakan sungai di Ibu Kota, airnya pun keruh dan banyak sampah tergenang sehingga menghasilkan bau yang tidak sedap. Saya pikir bau itu akan segera hilang ketika saya memasuki lingkungan sekolah. Namun ternyata tidak, di dalam lingkungan sekolah pun bau itu masih jelas tercium. Begini ternyata kehidupan sehari-hari di sekolah ini, belajar di tengah lingkungan Jakarta yang tidak sehat. Bahkan, tidak hanya di sekolah mereka harus &ldquo;bertarung&rdquo; dengan bau yang berasal dari sungai itu. Karena rumah mereka sebagian besar terletak di sekitar sungai, mereka juga harus merasakan aroma bau sungai di rumah mereka. </span></p>\r\n<p><strong><strong>&nbsp;<img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/907/QbzTJk.jpg" alt="" width="287" height="215" /></strong></strong></p>\r\n<p><span style="font-weight: 400;">Sehari-harinya, tak sedikit dari siswa SD tersebut yang menjadi pengamen di pagi hari dan baru sekolah di sore hari. Sungguh dapat kita bayangkan perjuangan mereka untuk menuntut ilmu. Jika ada murid yang tidak masuk dalam waktu yang lama, beberapa guru menghampiri rumah murid tersebut. Di sana, para guru menyaksikan langsung rumah-rumah petak sederhana mereka. Secara langsung, para guru juga mendengar secara langsung kisah anak-anak sekolah yang membantu orang tua yang berprofesi sebagai tukang parkir atau pengamen untuk memenuhi kebutuhan keluarga. Berdasarkan pertimbangan tersebut, akhirnya murid SD tersebut menjadi sasaran bagi Komunitas Ramadhan For Kids. Melalui bantuan guru di sana, kami meminta data siswa yang membutuhkan bantuan dari kami. </span></p>\r\n<p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/912/dsS6XZ.jpg" alt="" width="375" height="281" /></p>\r\n<p><span style="font-weight: 400;">Hari itu kami membawa paket baju dan alat shalat untuk dibagikan kepada siswa yang sudah terdata. Karena sekolah sudah mulai libur, awalnya kami berniat membagikan langsung ke rumah-rumah mereka saja. Namun, menurut salah seorang guru di sana, sebaiknya dipanggil saja muridnya agar tidak menimbulkan kecemburuan sosial bagi tetangga mereka yang tidak mendapatkan paket. Akhirnya salah seorang guru berinisiatif untuk berjalan ke rumah-rumah mereka untuk memanggil mereka ke sekolah. Dibantu dengan beberapa anak yang sudah dihampiri rumahnya, akhirnya terkumpullah siswa yang akan menerima paket. Tidak semuanya memang, sebagian lainnya akan diantar langsung oleh Ibu guru tersebut karena rumah mereka cukup jauh dari sekolah. Alhamdulillah, Komunitas Ramadhan For Kids mendapat dukungan dan bantuan yang sangat baik dari pihak sekolah sehingga paket bisa disalurkan kepada yang berhak menerima. Bahkan sebelum dibagikan paket yang sudah kami beri nama setiap anak itu dicek kembali apakah bajunya muat atau tidak. Jika kekecilan akan ditukar dengan paket baju milik temannya. Ya, kami hanya menerima data usia, sehingga jika tinggi siswanya melebihi rentang usianya mungkin saja kekecilan. :)</span></p>\r\n<p><strong><strong>&nbsp;</strong></strong></p>\r\n<p><span style="font-weight: 400;">Di akhir acara, guru agama memimpin doa untuk para donatur Ramadhan For Kids agar senantiasa dilapangkan rezekinya. Senyum merekah dari wajah adik-adik itu. Mereka mencium tangan guru dan tim Ramadhan For Kids satu per satu. Bersyukur, ya harusnya kita bersyukur bisa sekolah di tempat yang nyaman, tanpa ada bau tidak sedap yang mengganggu. Harusnya kita bersyukur memiliki rumah di lingkungan yang juga &nbsp;nyaman. Tak terbayangkan menjadi mereka, yang setiap harinya harus tinggal dan sekolah di lingkungan yang kumuh, ditambah menanggung beban ekonomi keluarga. </span></p>\r\n<p><strong><br /><span style="font-weight: 400;">Terima Kasih Kakak-Kakak. Salam dari Keluarga Besar SDN Manggarai Selatan 03 Pagi. (@mayaretno)</span></strong></p>	2016
2	2015-10-15 10:44:17+07	t	Berbuka Puasa Bersama Komunitas Sahabat Anak	<p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/911/QO9Tls.jpg" alt="" width="436" height="327" /></p>\r\n<p>Minggu, 5 Juli 2015 adalah giliran Suci dan Dwi untuk berjalan ke Manggarai! Kali ini, kami ditemani oleh Saudari kami Audry Pragita untuk mengantarkan paket donasi RamadhanForKids ke komunitas Sahabat Anak. Komunitas Sahabat Anak adalah sebuah komunitas peduli anak jalanan yang berlokasi dekat Stasiun Manggarai, Jakarta.</p>\r\n<p><img src="http://imagizer.imageshack.us/v2/640x480q90/910/xMEOi1.jpg" alt="" width="235" height="418" /></p>\r\n<p><span style="font-weight: 400;">Pukul 4 sore kami mulai berangkat dari Depok. Hari itu paket donasi rencananya akan dibagikan bertepatan dengan acara buka puasa bersama yang sedang dilaksanakan oleh Sahabat Anak. Meskipun hari itu kami sempat terjebak macet dan salah jalan, Alhamdulillah ke-50 paket donasi berhasil disalurkan. Terima kasih atas kerja samanya kakak-kakak Sahabat Anak! Terima kasih juga Audry sudah membantu hari ini :) (Suci Fadhilah)</span></p>	2016
1	2015-10-15 10:54:20+07	t	Berbagi Bersama Komunitas Jendela	<p><span style="font-weight: 400;"><img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/633/kfbheC.jpg" alt="" width="395" height="296" /></span></p>\r\n<p><span style="font-weight: 400;">Pada hari Minggu, 28 Juni 2015, sekitar pukul 14.00, rombongan komunitas Ramadhan for Kids bertolak dari Depok menuju Manggarai. Tujuan kami kali ini adalah salah satu komunitas sosial di kawasan pemukiman dekat Stasiun Manggarai, Komunitas Jendela namanya. Komunitas ini bertempat di jalan Manggarai 6. Komunitas ini merupakan sebuah komunitas yang memfasilitasi anak-anak pemulung di daerah setempat untuk belajar dengan mendirikan taman baca. Komunitas ini memiliki beberapa cabang di beberapa tempat, salah satunya adalah di Manggarai yang kami kunjungi ini.</span></p>\r\n<p><strong><strong>&nbsp;<img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/910/DKVIdf.jpg" alt="" width="421" height="315" /></strong></strong></p>\r\n<p><span style="font-weight: 400;">Sore hari itu, Komunitas Jendela mengadakan acara buka bersama. Kami memilih momentum acara tersebut untuk menyampaikan amanah dari para donatur berupa paket baju lebaran dan alat shalat. Sekitar pukul 15.30, kami sampai di lokasi. Berhubung sudah waktu Ashar, kami memutuskan untuk shalat terlebih dahulu. Sebelum acara mulai, kami sempat melihat-lihat pemukiman sekitar dan bercengkerama bersama beberapa anak-anak di sana. Acara dimulai sekitar pukul 16.00. Acara dibuka dengan tilawah oleh salah satu anak PM (penerima manfaat) dari Komunitas Jendela. Acara dilanjutkan tanya kabar dan perkenalan dari penerima manfaat, kakak-kakak pengurus Komunitas Jendela, dan komunitas Ramadhan for Kids.</span></p>\r\n<p><strong><strong>&nbsp;<img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/633/g7NPzf.jpg" alt="" width="392" height="294" /></strong></strong></p>\r\n<p><span style="font-weight: 400;">Tiba saatnya pengurus Komunitas Jendela mempersilakan kami untuk menyerahkan bingkisan kepada anak-anak penerima manfaat. Penyerahan bingkisan dilakukan secara simbolis kepada dua orang anak, satu anak laki-laki dan satu anak perempuan. Kemudian, kami berfoto bersama. Pada kesempatan kali ini, ada 34 paket &nbsp;yang kami distribusikan ke komunitas ini. Usai penyerahan paket, kami pamit untuk pulang mengingat tugas kami sudah selesai. Kami juga harus mengejar waktu Maghrib untuk berbuka di rumah masing-masing. Pada pendistribusian paket kali ini, kami dibantu Kak Kirana yang berbaik hati menjemput kami untuk mengantarkan paket dan ikut bersama kami mendistibusikannya. Semoga Allah membalas kebaikannya dengan balasan yang lebih baik dan berlipat. Aamiin. (Dwi Teguh Priyantini)</span></p>\r\n<p>&nbsp;</p>	2016
6	2015-10-27 04:40:13+07	t	Bingkisan untuk Adik-Adik Kampung Gedong	<p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://imagizer.imageshack.us/v2/640x480q90/908/ZDCQ4o.jpg" alt="" width="640" height="480" /></p>\r\n<p><span style="font-weight: 400;">Edukasi Ciliwung merupakan sebuah komunitas anak yang terletak di sebuah kampung pinggir Kali Ciliwung, yaitu Kampung Gedong, Depok. Komunitas ini diinisiasi oleh Formasi FIB UI (lembaga dakwah di Fakultas Ilmu Budaya Universitas Indonesia). </span></p>\r\n<p><span style="font-weight: 400;">Alhamdulillah pada hari Minggu tanggal 5 Juli 2015, Edukasi Ciliwung mengadakan kegiatan pesantren kilat dan buka bersama. Kami dari komunitas Ramadhan For Kids datang dengan membawa sebanyak 30 paket C&nbsp; yang terdiri dari alat solat, baju baru dan al ma'surat. Lokasi Kampung Gedong yang terletak cukup dari jalan utama dan harus melewati gang-gang kecil dan terjal membuat perjalanan ini sangat mengesankan. Dengan dibantu oleh dua sahabat lainnya, kami menggunakan motor berhasil sampai di lokasi sekitar pukul 15.30. Pada saat kami sampai di lokasi, acara pesantren kilat sedang berlangsung. Beberapa kakak sedang memberikan materi, sementara anak-anak duduk dengan hikmat mendengarkan. Bau limbah dari sungai yang dapat mengganggu konsentrasi tak mereka pedulikan. </span></p>\r\n<p><span style="font-weight: 400;">Menjelang maghrib, kami memperkenalkan diri sembari membagikan bingkisan. Anak-anak begitu bahagia menerima bingkisan tersebut. Alhamdulillah, pembagian paket selesai tepat dengan datangnya waktu berbuka puasa. Sebanyak 24 paket diterima oleh anak-anak secara langsung dan sebagian yang lainnya ada yang di titipkan ke penduduk sekitar.</span></p>\r\n<p><strong><span style="font-weight: 400;">Masih ada 6 paket bingkisan lagi yang belum dibagikan. Di perjalanan pulang, kami bertemu dengan bapak dan anak yang sedang berjalan dengan gerobaknya di Jalan Margonda. Tanpa ragu, kami salurkan langsung paket tersebut kepada mereka sebanyak 4 paket bingkisan. Dua paket lagi kami bawa pulang untuk dibagikan ke komunitas lain. Betapa bersyukur kami atas semua anugrah yang Allah berikan, sehingga kami dipertemukan kepada para penerima manfaat untuk menyalurkan amanah dari para donatur. Semoga Allah berikan sebaik-baiknya balasan kepada para donatur semua. Aamiin ya Allah. (Nunung Mulyani)</span></strong></p>\r\n<p>&nbsp;</p>	2016
17	2018-05-09 09:46:19.142499+07	f	Tes	<p>Tes</p>	2018
\.


--
-- Name: ramadhan4kidsapp_laporan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_laporan_id_seq', 17, true);


--
-- Data for Name: ramadhan4kidsapp_penerimamanfaat; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_penerimamanfaat (id, nama, pic, jumlah_anak) FROM stdin;
\.


--
-- Name: ramadhan4kidsapp_penerimamanfaat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_penerimamanfaat_id_seq', 1, false);


--
-- Data for Name: ramadhan4kidsapp_pengeluaran; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_pengeluaran (id, tanggal, keterangan, jumlah, untuk_donasi) FROM stdin;
\.


--
-- Name: ramadhan4kidsapp_pengeluaran_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_pengeluaran_id_seq', 1, false);


--
-- Data for Name: ramadhan4kidsapp_program; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_program (tema, tahun) FROM stdin;
Happy with Islam	2016
Happy Little Soul	2017
1000 Langkah Penuh Berkah	2018
\.


--
-- Data for Name: ramadhan4kidsapp_temp; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_temp (id, filecontent) FROM stdin;
\.


--
-- Name: ramadhan4kidsapp_temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_temp_id_seq', 1, false);


--
-- Data for Name: ramadhan4kidsapp_tipedonasi; Type: TABLE DATA; Schema: public; Owner: ramadhan4kids
--

COPY ramadhan4kidsapp_tipedonasi (id, tipe, nama, nominal, program_id) FROM stdin;
1	a	\N	60000	\N
2	c	\N	150000	\N
3	b	\N	100000	\N
5	a	Happy Praying	150000	2017
6	b	Happy Learning	100000	2017
7	c	Happy Growing	75000	2017
8	a	Happy School	50000	2018
9	b	Happy Feet	125000	2018
\.


--
-- Name: ramadhan4kidsapp_tipedonasi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramadhan4kids
--

SELECT pg_catalog.setval('ramadhan4kidsapp_tipedonasi_id_seq', 9, true);


--
-- Name: administrator_akun_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY administrator_akun
    ADD CONSTRAINT administrator_akun_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: ramadhan4kidsapp_anak_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_anak
    ADD CONSTRAINT ramadhan4kidsapp_anak_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_bank_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_bank
    ADD CONSTRAINT ramadhan4kidsapp_bank_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_donasi_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_donasi
    ADD CONSTRAINT ramadhan4kidsapp_donasi_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_donasidonatur_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_donasidonatur
    ADD CONSTRAINT ramadhan4kidsapp_donasidonatur_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_donatur_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_donatur
    ADD CONSTRAINT ramadhan4kidsapp_donatur_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_foto_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_foto
    ADD CONSTRAINT ramadhan4kidsapp_foto_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_kategorianak_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_kategorianak
    ADD CONSTRAINT ramadhan4kidsapp_kategorianak_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_laporan_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_laporan
    ADD CONSTRAINT ramadhan4kidsapp_laporan_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_penerimamanfaat_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_penerimamanfaat
    ADD CONSTRAINT ramadhan4kidsapp_penerimamanfaat_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_pengeluaran_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_pengeluaran
    ADD CONSTRAINT ramadhan4kidsapp_pengeluaran_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_program_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_program
    ADD CONSTRAINT ramadhan4kidsapp_program_pkey PRIMARY KEY (tahun);


--
-- Name: ramadhan4kidsapp_temp_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_temp
    ADD CONSTRAINT ramadhan4kidsapp_temp_pkey PRIMARY KEY (id);


--
-- Name: ramadhan4kidsapp_tipedonasi_pkey; Type: CONSTRAINT; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

ALTER TABLE ONLY ramadhan4kidsapp_tipedonasi
    ADD CONSTRAINT ramadhan4kidsapp_tipedonasi_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: ramadhan4kidsapp_anak_kategori_id_8bb25ad8; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_anak_kategori_id_8bb25ad8 ON ramadhan4kidsapp_anak USING btree (kategori_id);


--
-- Name: ramadhan4kidsapp_anak_komunitas_id_a370f724; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_anak_komunitas_id_a370f724 ON ramadhan4kidsapp_anak USING btree (komunitas_id);


--
-- Name: ramadhan4kidsapp_bank_program_id_4690d773; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_bank_program_id_4690d773 ON ramadhan4kidsapp_bank USING btree (program_id);


--
-- Name: ramadhan4kidsapp_bank_program_id_4690d773_like; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_bank_program_id_4690d773_like ON ramadhan4kidsapp_bank USING btree (program_id varchar_pattern_ops);


--
-- Name: ramadhan4kidsapp_donasi_donasi_donatur_id_2c166fd5; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_donasi_donasi_donatur_id_2c166fd5 ON ramadhan4kidsapp_donasi USING btree (donasi_donatur_id);


--
-- Name: ramadhan4kidsapp_donasi_program_id_756b2514; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_donasi_program_id_756b2514 ON ramadhan4kidsapp_donasi USING btree (program_id);


--
-- Name: ramadhan4kidsapp_donasi_program_id_756b2514_like; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_donasi_program_id_756b2514_like ON ramadhan4kidsapp_donasi USING btree (program_id varchar_pattern_ops);


--
-- Name: ramadhan4kidsapp_donasi_tipe_donasi_id_af4cad2a; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_donasi_tipe_donasi_id_af4cad2a ON ramadhan4kidsapp_donasi USING btree (tipe_donasi_id);


--
-- Name: ramadhan4kidsapp_donasidonatur_bank_yg_dituju_id_3118acff; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_donasidonatur_bank_yg_dituju_id_3118acff ON ramadhan4kidsapp_donasidonatur USING btree (bank_yg_dituju_id);


--
-- Name: ramadhan4kidsapp_donasidonatur_donatur_id_a4094369; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_donasidonatur_donatur_id_a4094369 ON ramadhan4kidsapp_donasidonatur USING btree (donatur_id);


--
-- Name: ramadhan4kidsapp_foto_laporan_id_157af3d0; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_foto_laporan_id_157af3d0 ON ramadhan4kidsapp_foto USING btree (laporan_id);


--
-- Name: ramadhan4kidsapp_laporan_program_id_4b42debb; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_laporan_program_id_4b42debb ON ramadhan4kidsapp_laporan USING btree (program_id);


--
-- Name: ramadhan4kidsapp_laporan_program_id_4b42debb_like; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_laporan_program_id_4b42debb_like ON ramadhan4kidsapp_laporan USING btree (program_id varchar_pattern_ops);


--
-- Name: ramadhan4kidsapp_program_tahun_479fe684_like; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_program_tahun_479fe684_like ON ramadhan4kidsapp_program USING btree (tahun varchar_pattern_ops);


--
-- Name: ramadhan4kidsapp_tipedonasi_program_id_d8a04af6; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_tipedonasi_program_id_d8a04af6 ON ramadhan4kidsapp_tipedonasi USING btree (program_id);


--
-- Name: ramadhan4kidsapp_tipedonasi_program_id_d8a04af6_like; Type: INDEX; Schema: public; Owner: ramadhan4kids; Tablespace: 
--

CREATE INDEX ramadhan4kidsapp_tipedonasi_program_id_d8a04af6_like ON ramadhan4kidsapp_tipedonasi USING btree (program_id varchar_pattern_ops);


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_ana_kategori_id_8bb25ad8_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_anak
    ADD CONSTRAINT ramadhan4kidsapp_ana_kategori_id_8bb25ad8_fk_ramadhan4 FOREIGN KEY (kategori_id) REFERENCES ramadhan4kidsapp_kategorianak(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_ana_komunitas_id_a370f724_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_anak
    ADD CONSTRAINT ramadhan4kidsapp_ana_komunitas_id_a370f724_fk_ramadhan4 FOREIGN KEY (komunitas_id) REFERENCES ramadhan4kidsapp_penerimamanfaat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_ban_program_id_4690d773_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_bank
    ADD CONSTRAINT ramadhan4kidsapp_ban_program_id_4690d773_fk_ramadhan4 FOREIGN KEY (program_id) REFERENCES ramadhan4kidsapp_program(tahun) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_don_bank_yg_dituju_id_3118acff_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_donasidonatur
    ADD CONSTRAINT ramadhan4kidsapp_don_bank_yg_dituju_id_3118acff_fk_ramadhan4 FOREIGN KEY (bank_yg_dituju_id) REFERENCES ramadhan4kidsapp_bank(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_don_donasi_donatur_id_2c166fd5_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_donasi
    ADD CONSTRAINT ramadhan4kidsapp_don_donasi_donatur_id_2c166fd5_fk_ramadhan4 FOREIGN KEY (donasi_donatur_id) REFERENCES ramadhan4kidsapp_donasidonatur(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_don_donatur_id_a4094369_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_donasidonatur
    ADD CONSTRAINT ramadhan4kidsapp_don_donatur_id_a4094369_fk_ramadhan4 FOREIGN KEY (donatur_id) REFERENCES ramadhan4kidsapp_donatur(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_don_program_id_756b2514_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_donasi
    ADD CONSTRAINT ramadhan4kidsapp_don_program_id_756b2514_fk_ramadhan4 FOREIGN KEY (program_id) REFERENCES ramadhan4kidsapp_program(tahun) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_don_tipe_donasi_id_af4cad2a_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_donasi
    ADD CONSTRAINT ramadhan4kidsapp_don_tipe_donasi_id_af4cad2a_fk_ramadhan4 FOREIGN KEY (tipe_donasi_id) REFERENCES ramadhan4kidsapp_tipedonasi(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_fot_laporan_id_157af3d0_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_foto
    ADD CONSTRAINT ramadhan4kidsapp_fot_laporan_id_157af3d0_fk_ramadhan4 FOREIGN KEY (laporan_id) REFERENCES ramadhan4kidsapp_laporan(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_lap_program_id_4b42debb_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_laporan
    ADD CONSTRAINT ramadhan4kidsapp_lap_program_id_4b42debb_fk_ramadhan4 FOREIGN KEY (program_id) REFERENCES ramadhan4kidsapp_program(tahun) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ramadhan4kidsapp_tip_program_id_d8a04af6_fk_ramadhan4; Type: FK CONSTRAINT; Schema: public; Owner: ramadhan4kids
--

ALTER TABLE ONLY ramadhan4kidsapp_tipedonasi
    ADD CONSTRAINT ramadhan4kidsapp_tip_program_id_d8a04af6_fk_ramadhan4 FOREIGN KEY (program_id) REFERENCES ramadhan4kidsapp_program(tahun) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

